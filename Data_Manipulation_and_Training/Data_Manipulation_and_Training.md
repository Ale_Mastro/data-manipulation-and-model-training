# OKR of Data Manipulation and Model Training
Introduction to the World of Data Science: **Data Manipulation and Model Training with Python**

This Python notebook is designed to help anyone wishing to approach the world of Data Science by focusing on data manipulation and model training. Regardless of your level of experience or background, here you will find a step-by-step guide to acquiring the skills you need to become a data scientist.

First of all, we will focus on _data manipulation_.
You will learn how to import data from different sources and how to perform **filtering**, **cleaning** and **transformation** operations to get a clean dataset ready for analysis. We will use libraries such as _Pandas_ and _NumPy_, which offer powerful tools for data management and processing.

Once we have mastered data manipulation skills, we will move on to the phase of building predictive models. We will explore the fundamental concepts of machine learning, focusing on supervised machine learning. You will learn how to prepare data for model training and use the _scikit-learn_ library to create **regression** and **classification** models.

We will not only stop at creating the models, but also learn how to accurately **evaluate** them to measure their performance. We will explore different evaluation measures, such as _accuracy_, _precision_, and _F1-score_, to evaluate the effectiveness of our models.

### Library import
In the context of a Jupyter notebook, the *!* (exclamation mark) symbol before a command in a line indicates that the next command should be executed as a system or shell command.

When you use the "!" symbol, the command that follows is executed in the terminal or shell of the operating system instead of in the Python environment of the notebook. This allows you to run system commands as if you were on the command line of your computer.


```python
!pip install matplotlib
```

    Requirement already satisfied: matplotlib in ./.venv/lib/python3.9/site-packages (3.7.1)
    Requirement already satisfied: contourpy>=1.0.1 in ./.venv/lib/python3.9/site-packages (from matplotlib) (1.1.0)
    Requirement already satisfied: cycler>=0.10 in ./.venv/lib/python3.9/site-packages (from matplotlib) (0.11.0)
    Requirement already satisfied: fonttools>=4.22.0 in ./.venv/lib/python3.9/site-packages (from matplotlib) (4.40.0)
    Requirement already satisfied: kiwisolver>=1.0.1 in ./.venv/lib/python3.9/site-packages (from matplotlib) (1.4.4)
    Requirement already satisfied: numpy>=1.20 in ./.venv/lib/python3.9/site-packages (from matplotlib) (1.24.3)
    Requirement already satisfied: packaging>=20.0 in ./.venv/lib/python3.9/site-packages (from matplotlib) (23.1)
    Requirement already satisfied: pillow>=6.2.0 in ./.venv/lib/python3.9/site-packages (from matplotlib) (9.5.0)
    Requirement already satisfied: pyparsing>=2.3.1 in ./.venv/lib/python3.9/site-packages (from matplotlib) (3.1.0)
    Requirement already satisfied: python-dateutil>=2.7 in ./.venv/lib/python3.9/site-packages (from matplotlib) (2.8.2)
    Requirement already satisfied: importlib-resources>=3.2.0 in ./.venv/lib/python3.9/site-packages (from matplotlib) (5.12.0)
    Requirement already satisfied: zipp>=3.1.0 in ./.venv/lib/python3.9/site-packages (from importlib-resources>=3.2.0->matplotlib) (3.15.0)
    Requirement already satisfied: six>=1.5 in ./.venv/lib/python3.9/site-packages (from python-dateutil>=2.7->matplotlib) (1.16.0)



```python
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime

with pd.option_context('display.max_rows', 10, 'display.max_columns', 5):
    pass
```

This line is used to show all the columns of our dataset without truncate them:
```bash
with pd.option_context('display.max_rows', 10, 'display.max_columns', 5):
    pass
```

### Import of the dataset
The dataset we are going to use was taken from the following platform https://www.kaggle.com/datasets/jealousleopard/goodreadsbooks

We are going to import the dataset in csv format using pandas, converting it into a dataframe.
```bash on_bad_lines='skip'``` is necessary due to the fact that are present some lines corrupted.

But, what is a **dataframe**?

_A DataFrame is a two-dimensional data structure that organizes data in a table of rows and columns, similar to a spreadsheet. It is one of the most common data types used in modern data analysis because it provides a flexible and intuitive way to store and work with data._

_Each DataFrame contains a structure, called a schema, which defines the name and data type of each column. It can contain universal data types such as StringType and IntegerType, as well as data types specific to a particular framework or library. Missing or incomplete values are stored as null values in the DataFrame._

_A DataFrame can be thought of as a spreadsheet with named columns. However, the main difference is that while a spreadsheet resides on a single computer in a specific location, a DataFrame can be distributed across thousands of computers. This makes it possible to analyze big data using distributed computing clusters. The use of multiple computers is necessary when the data is too large to be handled by a single computer or when processing would take too long._


```python
data = pd.read_csv("books.csv", on_bad_lines='skip')
data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>title</th>
      <th>authors</th>
      <th>average_rating</th>
      <th>isbn</th>
      <th>isbn13</th>
      <th>language_code</th>
      <th>num_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>publication_date</th>
      <th>publisher</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>9/16/2006</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>9/1/2004</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>J.K. Rowling</td>
      <td>4.42</td>
      <td>0439554896</td>
      <td>9780439554893</td>
      <td>eng</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>11/1/2003</td>
      <td>Scholastic</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5</td>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.56</td>
      <td>043965548X</td>
      <td>9780439655484</td>
      <td>eng</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>5/1/2004</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.78</td>
      <td>0439682584</td>
      <td>9780439682589</td>
      <td>eng</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>9/13/2004</td>
      <td>Scholastic</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>45631</td>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>William T. Vollmann/Larry McCaffery/Michael He...</td>
      <td>4.06</td>
      <td>1560254416</td>
      <td>9781560254416</td>
      <td>eng</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>12/21/2004</td>
      <td>Da Capo Press</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>45633</td>
      <td>You Bright and Risen Angels</td>
      <td>William T. Vollmann</td>
      <td>4.08</td>
      <td>0140110879</td>
      <td>9780140110876</td>
      <td>eng</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>12/1/1988</td>
      <td>Penguin Books</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>45634</td>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>William T. Vollmann</td>
      <td>3.96</td>
      <td>0140131965</td>
      <td>9780140131963</td>
      <td>eng</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>8/1/1993</td>
      <td>Penguin Books</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>45639</td>
      <td>Poor People</td>
      <td>William T. Vollmann</td>
      <td>3.72</td>
      <td>0060878827</td>
      <td>9780060878825</td>
      <td>eng</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>2/27/2007</td>
      <td>Ecco</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>45641</td>
      <td>Las aventuras de Tom Sawyer</td>
      <td>Mark Twain</td>
      <td>3.91</td>
      <td>8497646983</td>
      <td>9788497646987</td>
      <td>spa</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>5/28/2006</td>
      <td>Edimat Libros</td>
    </tr>
  </tbody>
</table>
<p>11123 rows × 12 columns</p>
</div>



We can also see some information about the nature of the data


```python
data.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 11123 entries, 0 to 11122
    Data columns (total 12 columns):
     #   Column              Non-Null Count  Dtype  
    ---  ------              --------------  -----  
     0   bookID              11123 non-null  int64  
     1   title               11123 non-null  object 
     2   authors             11123 non-null  object 
     3   average_rating      11123 non-null  float64
     4   isbn                11123 non-null  object 
     5   isbn13              11123 non-null  int64  
     6   language_code       11123 non-null  object 
     7     num_pages         11123 non-null  int64  
     8   ratings_count       11123 non-null  int64  
     9   text_reviews_count  11123 non-null  int64  
     10  publication_date    11123 non-null  object 
     11  publisher           11123 non-null  object 
    dtypes: float64(1), int64(5), object(6)
    memory usage: 1.0+ MB


As we can see there are some things that could cause problems during data analysis:
- The num_pages columns has a white space on the left
- Publication date is not a real date, but is parse as a String

This is an example of "errors" that can be fixed during the data preprocessing phase.
We can also quickly calculate some metrics that can give us an idea of the actual distribution of data for each column, also called **feature**


```python
data.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>average_rating</th>
      <th>isbn13</th>
      <th>num_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>11123.000000</td>
      <td>11123.000000</td>
      <td>1.112300e+04</td>
      <td>11123.000000</td>
      <td>1.112300e+04</td>
      <td>11123.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>21310.856963</td>
      <td>3.934075</td>
      <td>9.759880e+12</td>
      <td>336.405556</td>
      <td>1.794285e+04</td>
      <td>542.048099</td>
    </tr>
    <tr>
      <th>std</th>
      <td>13094.727252</td>
      <td>0.350485</td>
      <td>4.429758e+11</td>
      <td>241.152626</td>
      <td>1.124992e+05</td>
      <td>2576.619589</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>8.987060e+09</td>
      <td>0.000000</td>
      <td>0.000000e+00</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>10277.500000</td>
      <td>3.770000</td>
      <td>9.780345e+12</td>
      <td>192.000000</td>
      <td>1.040000e+02</td>
      <td>9.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>20287.000000</td>
      <td>3.960000</td>
      <td>9.780582e+12</td>
      <td>299.000000</td>
      <td>7.450000e+02</td>
      <td>47.000000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>32104.500000</td>
      <td>4.140000</td>
      <td>9.780872e+12</td>
      <td>416.000000</td>
      <td>5.000500e+03</td>
      <td>238.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>45641.000000</td>
      <td>5.000000</td>
      <td>9.790008e+12</td>
      <td>6576.000000</td>
      <td>4.597666e+06</td>
      <td>94265.000000</td>
    </tr>
  </tbody>
</table>
</div>



## Pre-processing phase:
We noticed that inconsistency within the num_pages column, this can be fixed by renaming the columns


```python
new_column_names = {
    'bookID': 'bookID',
    'title': 'title',
    'authors': 'authors',
    'average_rating': 'average_rating',
    'isbn': 'isbn',
    'isbn13': 'isbn13',
    'language_code': 'language_code',
    '  num_pages':'num_of_pages',
    'ratings_count': 'ratings_count',
    'text_reviews_count': 'text_reviews_count',
    'publication_date': 'publication_date',
    'publisher': 'publisher'
}

data = data.rename(columns=new_column_names)
data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>title</th>
      <th>authors</th>
      <th>average_rating</th>
      <th>isbn</th>
      <th>isbn13</th>
      <th>language_code</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>publication_date</th>
      <th>publisher</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>9/16/2006</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>9/1/2004</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>J.K. Rowling</td>
      <td>4.42</td>
      <td>0439554896</td>
      <td>9780439554893</td>
      <td>eng</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>11/1/2003</td>
      <td>Scholastic</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5</td>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.56</td>
      <td>043965548X</td>
      <td>9780439655484</td>
      <td>eng</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>5/1/2004</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.78</td>
      <td>0439682584</td>
      <td>9780439682589</td>
      <td>eng</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>9/13/2004</td>
      <td>Scholastic</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>45631</td>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>William T. Vollmann/Larry McCaffery/Michael He...</td>
      <td>4.06</td>
      <td>1560254416</td>
      <td>9781560254416</td>
      <td>eng</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>12/21/2004</td>
      <td>Da Capo Press</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>45633</td>
      <td>You Bright and Risen Angels</td>
      <td>William T. Vollmann</td>
      <td>4.08</td>
      <td>0140110879</td>
      <td>9780140110876</td>
      <td>eng</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>12/1/1988</td>
      <td>Penguin Books</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>45634</td>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>William T. Vollmann</td>
      <td>3.96</td>
      <td>0140131965</td>
      <td>9780140131963</td>
      <td>eng</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>8/1/1993</td>
      <td>Penguin Books</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>45639</td>
      <td>Poor People</td>
      <td>William T. Vollmann</td>
      <td>3.72</td>
      <td>0060878827</td>
      <td>9780060878825</td>
      <td>eng</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>2/27/2007</td>
      <td>Ecco</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>45641</td>
      <td>Las aventuras de Tom Sawyer</td>
      <td>Mark Twain</td>
      <td>3.91</td>
      <td>8497646983</td>
      <td>9788497646987</td>
      <td>spa</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>5/28/2006</td>
      <td>Edimat Libros</td>
    </tr>
  </tbody>
</table>
<p>11123 rows × 12 columns</p>
</div>



We also go on to convert publication_date from string to date.

data['publication_date'] = pd.to_datetime(data['publication_date'])

We received an error, this can be seen from the log that says having a problem with line 8177. We use **.iloc[< line number >]** to access the line and we can notice that the 31th of November doesn't exist.


```python
data.iloc[8177]
```




    bookID                                                            31373
    title                 In Pursuit of the Proper Sinner (Inspector Lyn...
    authors                                               Elizabeth  George
    average_rating                                                      4.1
    isbn                                                         0553575104
    isbn13                                                    9780553575101
    language_code                                                       eng
    num_of_pages                                                        718
    ratings_count                                                     10608
    text_reviews_count                                                  295
    publication_date                                             11/31/2000
    publisher                                                  Bantam Books
    Name: 8177, dtype: object



Our approach in this case will be to go in and eliminate all lines that contain out-of-range values when converting to dates


```python
# Convert 'date' column to datetime type if it is not already
data['publication_date'] = pd.to_datetime(data['publication_date'], errors='coerce')

# Filters rows excluding those with day out of range
data = data[pd.DatetimeIndex(data['publication_date']).day <= pd.DatetimeIndex(data['publication_date']).days_in_month];

# Now the DataFrame contains only rows with valid dates
data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>title</th>
      <th>authors</th>
      <th>average_rating</th>
      <th>isbn</th>
      <th>isbn13</th>
      <th>language_code</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>publication_date</th>
      <th>publisher</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>2006-09-16</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>2004-09-01</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>J.K. Rowling</td>
      <td>4.42</td>
      <td>0439554896</td>
      <td>9780439554893</td>
      <td>eng</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>2003-11-01</td>
      <td>Scholastic</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5</td>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.56</td>
      <td>043965548X</td>
      <td>9780439655484</td>
      <td>eng</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>2004-05-01</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.78</td>
      <td>0439682584</td>
      <td>9780439682589</td>
      <td>eng</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>2004-09-13</td>
      <td>Scholastic</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>45631</td>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>William T. Vollmann/Larry McCaffery/Michael He...</td>
      <td>4.06</td>
      <td>1560254416</td>
      <td>9781560254416</td>
      <td>eng</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>2004-12-21</td>
      <td>Da Capo Press</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>45633</td>
      <td>You Bright and Risen Angels</td>
      <td>William T. Vollmann</td>
      <td>4.08</td>
      <td>0140110879</td>
      <td>9780140110876</td>
      <td>eng</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>1988-12-01</td>
      <td>Penguin Books</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>45634</td>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>William T. Vollmann</td>
      <td>3.96</td>
      <td>0140131965</td>
      <td>9780140131963</td>
      <td>eng</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>1993-08-01</td>
      <td>Penguin Books</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>45639</td>
      <td>Poor People</td>
      <td>William T. Vollmann</td>
      <td>3.72</td>
      <td>0060878827</td>
      <td>9780060878825</td>
      <td>eng</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>2007-02-27</td>
      <td>Ecco</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>45641</td>
      <td>Las aventuras de Tom Sawyer</td>
      <td>Mark Twain</td>
      <td>3.91</td>
      <td>8497646983</td>
      <td>9788497646987</td>
      <td>spa</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>2006-05-28</td>
      <td>Edimat Libros</td>
    </tr>
  </tbody>
</table>
<p>11121 rows × 12 columns</p>
</div>



## Data Analysis:
Seeing the dataframe it is possible to ask several questions including:
- What are the unique values?
- Are there any NaN values?
- Which author wrote the most books?
- What is the most reprinted book?
- What is the book with the highest rating?
- Is the dataset homogeneous? That is, is the distribution of the language feature well balanced for all languages?
- Which author has the highest average rating?
- How are the various features distributed?

### Unique Values:
The only values that may be of interest to us are those concerning titles and authors.

We are going to use the **.unique()** over a dataframe's column.


```python
unique_titles = data['title'].unique()
# Len() is to get the actual lenght of the list
len(unique_titles), len(data['title'])
```




    (10346, 11121)



For authors it is a bit different in that some are more authors separated by /.  For the purposes of this notebook we will keep the authors joined by the / but to count unique authors we are going to split them and duplicate the rows.

In order not to contaminate the data we are going to analyze later, let's make a copy of our dataframe with the **.copy()** function


```python
data_with_exploded_auth = data.copy()
```

We are going to reassign to each row in the authors column the authors split by /.


```python
data_with_exploded_auth['authors'] = [x.split('/') for x in data['authors']]
```

_a bit of context_ : the upper code is a list comprehension that splits the values in the 'authors' column of the DataFrame 'data' based on the '/' delimiter.
- **data['authors']** retrieves the values in the 'authors' column of the DataFrame 'data'. It assumes that 'data' is a DataFrame with a column named 'authors'.

- The list comprehension **[x.split('/') for x in data['authors']]** iterates over each value in the 'authors' column and applies the **split('/')** method to each value.

- **x.split('/')** splits each value in the 'authors' column based on the '/' delimiter. It returns a list of substrings obtained by splitting the original string.

- Finally, the list comprehension collects the results of the split('/') operation for each value in the 'authors' column, creating a new list with the split substrings for each value. The resulting list will have the same number of elements as the 'authors' column, where each element is a list of substrings obtained by splitting the original value using '/' as the delimiter.

In summary, the code splits the values in the 'authors' column using the '/' delimiter, creating a new list of lists where each inner list contains the split substrings.

And finally we explode the lines, this means that for each value in the array of a row in authors the row is duplicated and assigned an array author


```python
data_with_exploded_auth = data_with_exploded_auth.explode('authors')
data_with_exploded_auth
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>title</th>
      <th>authors</th>
      <th>average_rating</th>
      <th>isbn</th>
      <th>isbn13</th>
      <th>language_code</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>publication_date</th>
      <th>publisher</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>J.K. Rowling</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>2006-09-16</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>Mary GrandPré</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>2006-09-16</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>J.K. Rowling</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>2004-09-01</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>Mary GrandPré</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>2004-09-01</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>J.K. Rowling</td>
      <td>4.42</td>
      <td>0439554896</td>
      <td>9780439554893</td>
      <td>eng</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>2003-11-01</td>
      <td>Scholastic</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>45631</td>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>Michael Hemmingson</td>
      <td>4.06</td>
      <td>1560254416</td>
      <td>9781560254416</td>
      <td>eng</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>2004-12-21</td>
      <td>Da Capo Press</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>45633</td>
      <td>You Bright and Risen Angels</td>
      <td>William T. Vollmann</td>
      <td>4.08</td>
      <td>0140110879</td>
      <td>9780140110876</td>
      <td>eng</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>1988-12-01</td>
      <td>Penguin Books</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>45634</td>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>William T. Vollmann</td>
      <td>3.96</td>
      <td>0140131965</td>
      <td>9780140131963</td>
      <td>eng</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>1993-08-01</td>
      <td>Penguin Books</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>45639</td>
      <td>Poor People</td>
      <td>William T. Vollmann</td>
      <td>3.72</td>
      <td>0060878827</td>
      <td>9780060878825</td>
      <td>eng</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>2007-02-27</td>
      <td>Ecco</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>45641</td>
      <td>Las aventuras de Tom Sawyer</td>
      <td>Mark Twain</td>
      <td>3.91</td>
      <td>8497646983</td>
      <td>9788497646987</td>
      <td>spa</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>2006-05-28</td>
      <td>Edimat Libros</td>
    </tr>
  </tbody>
</table>
<p>19234 rows × 12 columns</p>
</div>



As we can see we have a duplicated book ID 1, which has all the values duplicated except for the author.

### Checking for None/NaN values:


```python
# Check for None values
data.isnull().values.any()

# Check for NaN values
data.isna().values.any()

```




    False



## Knowing the dataset
Now we are going to answer some questions that will allow us to better understand how our dataset is structured and the distribution of some values

### What is the most reprinted book?


```python
top_20_books = data['title'].value_counts()[:20]
top_20_books
```




    title
    The Brothers Karamazov        9
    The Iliad                     9
    Anna Karenina                 8
    Gulliver's Travels            8
    'Salem's Lot                  8
    The Odyssey                   8
    The Picture of Dorian Gray    7
    A Midsummer Night's Dream     7
    Treasure Island               6
    Sense and Sensibility         6
    Jane Eyre                     6
    The Great Gatsby              6
    The Histories                 6
    The Scarlet Letter            6
    The Secret Garden             6
    Romeo and Juliet              6
    Robinson Crusoe               6
    Macbeth                       6
    Collected Stories             6
    Pride and Prejudice           5
    Name: count, dtype: int64



_A bit of context_: The code ```data['title'].value_counts()[:20]``` performs a frequency count of unique values in the 'title' column of the DataFrame 'data' and returns the top 20 most common values. Let's break down the code step by step:
```bash 
data['title']  # Accessing the 'title' column of the DataFrame 'data'
```
- **data['title']** retrieves the values in the 'title' column of the DataFrame 'data'. It assumes that 'data' is a DataFrame with a column named 'title'.
```python
data['title'].value_counts()
```
- **data['title'].value_counts()** calculates the frequency count of unique values in the 'title' column. It returns a new Pandas Series object where each unique value in the 'title' column is the index, and the corresponding value is the count of occurrences of that value in the column.
```bash 
data['title'].value_counts()[:20]
```
- **[:20]** is a slicing operation that selects the first 20 rows of the resulting Pandas Series. It returns a new Pandas Series object with only the top 20 most common values from the 'title' column, sorted in descending order of their frequency count.

Overall, the code data['title'].value_counts()[:20] provides you with the top 20 most frequently occurring values in the 'title' column of the DataFrame 'data'. It can be useful for identifying the most common titles or finding patterns in the data.






We also go on to plot our results using the previously imported library.

1 - _x_ is assigned the index of the top_20_books Pandas Series in reverse order. The [::-1] indexing syntax is used to reverse the order of the index.

2 - _y_ is assigned the top_20_books Pandas Series in reverse order. This ensures that the bars in the bar chart are displayed in descending order of their counts.)

3 - plt.barh(x, y) creates a horizontal bar chart using the barh function from matplotlib. It takes the reversed index x as the y-axis values and the reversed values y as the corresponding bar heights.


```python
x = top_20_books.index[::-1]
y = top_20_books[::-1] # [::-1] Reverse the list
plt.barh(x, y)
plt.xlabel('Count')
plt.ylabel('Books')
plt.title('Top 20 Rewrited Books');

```


    
![png](output_39_0.png)
    


###  What is the book with the highest rating?


```python
top_20_values = data.sort_values('ratings_count', ascending=False).head(20)
```

The code above ```top_20_values = data.sort_values('ratings_count', ascending=False).head(20)``` sorts the DataFrame 'data' based on the 'ratings_count' column in descending order and selects the top 20 rows. Let's break down the code step by step:
```python
data.sort_values('ratings_count', ascending=False)
```
- **sorts** the DataFrame 'data' based on the values in the 'ratings_count' column in descending order. This rearranges the rows of the DataFrame, placing the highest 'ratings_count' values at the top.
```python
data.sort_values('ratings_count', ascending=False).head(20)
```
- **.head(20)** is a method chained to the sorted DataFrame. It selects the first 20 rows from the sorted DataFrame, containing the highest 'ratings_count' values.
```python
top_20_values = data.sort_values('ratings_count', ascending=False).head(20)
```
- top_20_values is assigned the resulting DataFrame with the top 20 rows from 'data' based on the highest 'ratings_count' values.

Overall, the code creates a new DataFrame called 'top_20_values' that contains the top 20 rows from the original 'data' DataFrame. The rows are selected based on the highest 'ratings_count' values, as the DataFrame is sorted in descending order before the selection. This code can be useful for identifying and analyzing the 20 records with the highest 'ratings_count' in the dataset.


```python
y = top_20_values['ratings_count'][::-1]
x = top_20_values['title'][::-1]
plt.barh(x, y)
plt.xlabel('Count')
plt.ylabel('Books')
plt.title('Top 20 Rated Books')
plt.show()
```


    
![png](output_43_0.png)
    


### Which author wrote the most books?
For this task we use the previous dataset in which authors are splitted for the /


```python
all_unique_auth = data_with_exploded_auth['authors'].value_counts()[:20]
all_unique_auth
```




    authors
    Stephen King              99
    William Shakespeare       93
    J.R.R. Tolkien            55
    Sandra Brown              48
    Agatha Christie           47
    P.G. Wodehouse            47
    Roald Dahl                46
    Orson Scott Card          44
    Mercedes Lackey           43
    James Patterson           43
    Fyodor Dostoyevsky        40
    Rumiko Takahashi          39
    Margaret Weis             38
    Gabriel García Márquez    37
    Dean Koontz               35
    C.S. Lewis                35
    Terry Pratchett           34
    Charles Dickens           33
    Neil Gaiman               33
    Jane Austen               33
    Name: count, dtype: int64




```python
plt.barh(all_unique_auth.index[::-1], all_unique_auth[::-1])
plt.xlabel('Count')
plt.ylabel('Author')
plt.title('Top 20 authors who have published the most books')
plt.show()
```


    
![png](output_46_0.png)
    


### What is the author with the highest rating?

For the sake of simplicity and performance we are going to exclude all reviews that are below 4.3


```python
high_rated_author = data[data['average_rating'] >= 4.3]
```

The code ```high_rated_author = data[data['average_rating'] >= 4.3]``` creates a new DataFrame called 'high_rated_author' that contains rows from the original 'data' DataFrame where the 'average_rating' column is greater than or equal to 4.3. Let's break down the code step by step:
```python
data['average_rating'] >= 4.3
```
- This expression compares each value in the 'average_rating' column of the DataFrame 'data' with 4.3. It returns a boolean Series where each element is True if the corresponding value in 'average_rating' is greater than or equal to 4.3, and False otherwise.
```python
data[data['average_rating'] >= 4.3]
```
- data['average_rating'] >= 4.3 is used as a boolean indexing condition on the DataFrame 'data'. By applying this condition inside the square brackets, it filters the rows of the DataFrame and selects only those rows where the condition is True. This creates a new DataFrame called 'high_rated_author' that contains the rows from 'data' with an 'average_rating' greater than or equal to 4.3.

Just for fun i want to test polars:


```python
!pip install polars
```

    Requirement already satisfied: polars in ./.venv/lib/python3.9/site-packages (0.18.5)



```python
!pip install pyarrow==5.0.0
```

    Collecting pyarrow==5.0.0
      Using cached pyarrow-5.0.0-cp39-cp39-macosx_11_0_arm64.whl (12.3 MB)
    Requirement already satisfied: numpy>=1.16.6 in ./.venv/lib/python3.9/site-packages (from pyarrow==5.0.0) (1.24.3)
    Installing collected packages: pyarrow
      Attempting uninstall: pyarrow
        Found existing installation: pyarrow 12.0.1
        Uninstalling pyarrow-12.0.1:
          Successfully uninstalled pyarrow-12.0.1
    [31mERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.
    datasets 2.13.1 requires pyarrow>=8.0.0, but you have pyarrow 5.0.0 which is incompatible.[0m[31m
    [0mSuccessfully installed pyarrow-5.0.0



```python
import polars as pl
```


```python
high_rated_author = data[data['average_rating'] >= 4.3]
```


```python
high_rated_author = (pl.scan_csv('books.csv',ignore_errors=True).filter(pl.col('average_rating')>=4.3)).collect()
```


```python
%%time
high_rated_author = (
    high_rated_author.groupby("authors")
    .agg(pl.count("title"))
    .sort("title", descending=True)
    .head(20)
)
```

    CPU times: user 2.34 ms, sys: 7.21 ms, total: 9.55 ms
    Wall time: 6.62 ms



```python
high_rated_author
```




<div><style>
.dataframe > thead > tr > th,
.dataframe > tbody > tr > td {
  text-align: right;
}
</style>
<small>shape: (20, 2)</small><table border="1" class="dataframe"><thead><tr><th>authors</th><th>title</th></tr><tr><td>str</td><td>u32</td></tr></thead><tbody><tr><td>&quot;Hiromu Arakawa…</td><td>12</td></tr><tr><td>&quot;J.R.R. Tolkien…</td><td>12</td></tr><tr><td>&quot;J.K. Rowling&quot;</td><td>11</td></tr><tr><td>&quot;Tite Kubo&quot;</td><td>10</td></tr><tr><td>&quot;Hiromu Arakawa…</td><td>8</td></tr><tr><td>&quot;Patrick O&#x27;Bria…</td><td>8</td></tr><tr><td>&quot;Hirohiko Araki…</td><td>7</td></tr><tr><td>&quot;Bill Watterson…</td><td>7</td></tr><tr><td>&quot;P.G. Wodehouse…</td><td>7</td></tr><tr><td>&quot;Karen Kingsbur…</td><td>7</td></tr><tr><td>&quot;Hirohiko Araki…</td><td>6</td></tr><tr><td>&quot;J.K. Rowling/M…</td><td>6</td></tr><tr><td>&quot;Douglas Adams&quot;</td><td>5</td></tr><tr><td>&quot;Shel Silverste…</td><td>5</td></tr><tr><td>&quot;Karen Kingsbur…</td><td>5</td></tr><tr><td>&quot;Hiromu Arakawa…</td><td>5</td></tr><tr><td>&quot;Stephen King&quot;</td><td>5</td></tr><tr><td>&quot;Diana Gabaldon…</td><td>5</td></tr><tr><td>&quot;Thomas Sowell&quot;</td><td>5</td></tr><tr><td>&quot;Jane Austen&quot;</td><td>4</td></tr></tbody></table></div>




```python
high_rated_author = data[data['average_rating'] >= 4.3]
```


```python
%%time
high_rated_author = high_rated_author.groupby('authors')['title'].count().reset_index().sort_values('title', ascending=False).head(20)
```

    CPU times: user 1.84 ms, sys: 1.06 ms, total: 2.9 ms
    Wall time: 2.46 ms



```python
high_rated_author
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>authors</th>
      <th>title</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>269</th>
      <td>Hiromu Arakawa/Akira Watanabe</td>
      <td>12</td>
    </tr>
    <tr>
      <th>298</th>
      <td>J.R.R. Tolkien</td>
      <td>12</td>
    </tr>
    <tr>
      <th>289</th>
      <td>J.K. Rowling</td>
      <td>11</td>
    </tr>
    <tr>
      <th>694</th>
      <td>Tite Kubo</td>
      <td>10</td>
    </tr>
    <tr>
      <th>271</th>
      <td>Hiromu Arakawa/荒川弘/方郁仁</td>
      <td>8</td>
    </tr>
    <tr>
      <th>547</th>
      <td>Patrick O'Brian</td>
      <td>8</td>
    </tr>
    <tr>
      <th>402</th>
      <td>Karen Kingsbury</td>
      <td>7</td>
    </tr>
    <tr>
      <th>532</th>
      <td>P.G. Wodehouse</td>
      <td>7</td>
    </tr>
    <tr>
      <th>267</th>
      <td>Hirohiko Araki/Hirohiko Araki</td>
      <td>7</td>
    </tr>
    <tr>
      <th>64</th>
      <td>Bill Watterson</td>
      <td>7</td>
    </tr>
    <tr>
      <th>293</th>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>6</td>
    </tr>
    <tr>
      <th>266</th>
      <td>Hirohiko Araki</td>
      <td>6</td>
    </tr>
    <tr>
      <th>403</th>
      <td>Karen Kingsbury/Gary Smalley</td>
      <td>5</td>
    </tr>
    <tr>
      <th>690</th>
      <td>Thomas Sowell</td>
      <td>5</td>
    </tr>
    <tr>
      <th>643</th>
      <td>Shel Silverstein</td>
      <td>5</td>
    </tr>
    <tr>
      <th>656</th>
      <td>Stephen King</td>
      <td>5</td>
    </tr>
    <tr>
      <th>143</th>
      <td>Diana Gabaldon</td>
      <td>5</td>
    </tr>
    <tr>
      <th>153</th>
      <td>Douglas Adams</td>
      <td>5</td>
    </tr>
    <tr>
      <th>268</th>
      <td>Hiromu Arakawa</td>
      <td>5</td>
    </tr>
    <tr>
      <th>441</th>
      <td>Marcel Proust/C.K. Scott Moncrieff/Terence Kil...</td>
      <td>4</td>
    </tr>
  </tbody>
</table>
</div>



The code ```high_rated_author = high_rated_author.groupby('authors')['title'].count().reset_index().sort_values('title', ascending=False).head(20)``` performs a series of operations on the 'high_rated_author' DataFrame to group the data by authors, count the number of titles per author, sort the results in descending order based on the title count, and select the top 20 authors. Let's break down the code step by step:
```python
high_rated_author.groupby('authors')['title'].count()
```
- **groupby('authors')['title'].count()** groups the 'high_rated_author' DataFrame by the 'authors' column and then counts the number of titles for each author. This returns a new Series object where the authors are the index and the corresponding values represent the count of titles for each author.
```python
.reset_index()
```
- **.reset_index()** converts the resulting Series back into a DataFrame. This operation resets the index of the DataFrame and promotes the 'authors' column to a regular column.
```python
.sort_values('title', ascending=False)
```
- **.sort_values('title', ascending=False)** sorts the DataFrame based on the 'title' column in descending order. This rearranges the DataFrame, placing authors with the highest title counts at the top.
```python
.head(20)
```
- **.head(20)** selects the first 20 rows of the sorted DataFrame. It captures the top 20 authors with the highest title counts.
```python
high_rated_author = high_rated_author.groupby('authors')['title'].count().reset_index().sort_values('title', ascending=False).head(20)
```
- high_rated_author is assigned the resulting DataFrame with the top 20 authors and their corresponding title counts. The DataFrame is obtained by applying the aforementioned operations.

Overall, the code generates a new DataFrame called 'high_rated_author' that contains the top 20 authors with the highest title counts in the 'high_rated_author' DataFrame. This is achieved by grouping the data by authors, counting the titles per author, sorting the DataFrame based on the title counts, and selecting the top 20 rows. This can be helpful for identifying the most prolific authors within the subset of highly rated authors.







```python
plt.bar(high_rated_author['authors'], high_rated_author['title'])
plt.xlabel('Author')
plt.ylabel('Count')
plt.title('Authors Best Rate')
plt.xticks(rotation=90);  # Rotate x-axis labels by 45 degrees

```

    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/events.py:93: UserWarning: Glyph 33618 (\N{CJK UNIFIED IDEOGRAPH-8352}) missing from current font.
      func(*args, **kwargs)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/events.py:93: UserWarning: Glyph 24029 (\N{CJK UNIFIED IDEOGRAPH-5DDD}) missing from current font.
      func(*args, **kwargs)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/events.py:93: UserWarning: Glyph 24344 (\N{CJK UNIFIED IDEOGRAPH-5F18}) missing from current font.
      func(*args, **kwargs)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/events.py:93: UserWarning: Glyph 26041 (\N{CJK UNIFIED IDEOGRAPH-65B9}) missing from current font.
      func(*args, **kwargs)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/events.py:93: UserWarning: Glyph 37057 (\N{CJK UNIFIED IDEOGRAPH-90C1}) missing from current font.
      func(*args, **kwargs)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/events.py:93: UserWarning: Glyph 20161 (\N{CJK UNIFIED IDEOGRAPH-4EC1}) missing from current font.
      func(*args, **kwargs)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/pylabtools.py:152: UserWarning: Glyph 33618 (\N{CJK UNIFIED IDEOGRAPH-8352}) missing from current font.
      fig.canvas.print_figure(bytes_io, **kw)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/pylabtools.py:152: UserWarning: Glyph 24029 (\N{CJK UNIFIED IDEOGRAPH-5DDD}) missing from current font.
      fig.canvas.print_figure(bytes_io, **kw)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/pylabtools.py:152: UserWarning: Glyph 24344 (\N{CJK UNIFIED IDEOGRAPH-5F18}) missing from current font.
      fig.canvas.print_figure(bytes_io, **kw)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/pylabtools.py:152: UserWarning: Glyph 26041 (\N{CJK UNIFIED IDEOGRAPH-65B9}) missing from current font.
      fig.canvas.print_figure(bytes_io, **kw)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/pylabtools.py:152: UserWarning: Glyph 37057 (\N{CJK UNIFIED IDEOGRAPH-90C1}) missing from current font.
      fig.canvas.print_figure(bytes_io, **kw)
    /Users/alessandromastropasqua/Documents/okr/.venv/lib/python3.9/site-packages/IPython/core/pylabtools.py:152: UserWarning: Glyph 20161 (\N{CJK UNIFIED IDEOGRAPH-4EC1}) missing from current font.
      fig.canvas.print_figure(bytes_io, **kw)



    
![png](output_63_1.png)
    


### Is the dataset homogeneous? That is, is the distribution of the language feature well balanced for all languages?


```python
lng = data['language_code'].value_counts()
plt.bar(lng.index, lng)
plt.xlabel('Lang')
plt.ylabel('Count')
plt.title('Language Distribution')
plt.xticks(rotation=45)  # Rotate x-axis labels by 45 degrees

plt.show()
```


    
![png](output_65_0.png)
    


## How are the various features distributed?

Now we are going to visualize the distribution of features. Let's start by visualizing the distribution of average ratings for each author in order to identify outliers.
But why is it important to identify them?

**Outliers** within a training dataset can cause several problems during model creation and training. Here are some of the main problems associated with outliers:

- **Model parameter bias**: Outliers can significantly affect the estimation of model parameters during training. As machine learning models try to minimize the overall error, outliers can lead to biased estimation of model weights as the model will try to fit these outliers.

- **Overfitting**: Outliers can be interpreted by the model as highly significant points, leading to overfitting (overfitting) on the training data. This means that the model will be highly specialized for outlier points and may have a poor ability to generalize to new data. As a result, the model may produce inaccurate or unreliable predictions on previously unseen data.

- **Reduced model performance**: If a model is trained on a dataset containing outliers, its ability to learn the patterns and relationships in the data could be impaired. This could lead to reduced model performance in making predictions on normal data, as the model could be affected by incorrect information provided by the outliers.

- **Sensitivity to measurement errors**: Outliers can be caused by measurement errors or corrupted data. If the training dataset contains outliers resulting from measurement errors, the model may be sensitive to these errors and produce incorrect or unreliable predictions on new data.

- **Distorted distances and similarities**: Outliers can affect the measurement of distances or similarities between points in the dataset. For example, if an outlier is located far away from most points, it could adversely affect the distance-based similarity measure, making it difficult to correctly identify patterns or clusters in the data.

To mitigate the negative effects of outliers, several strategies can be adopted, such as detecting and removing outliers from the training dataset or using machine learning models that are robust to outliers. In addition, it is important to carefully examine the context of the problem and the nature of the data to determine whether outliers are indeed anomalous data or represent meaningful information for the machine learning task at hand.

Let's take the average rating for each author to start with and plot it with a scatter plot


```python
mean_per_auth = data.groupby('authors')['average_rating'].mean().reset_index()
mean_per_auth
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>authors</th>
      <th>average_rating</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>A.B. Yehoshua/Hillel Halkin</td>
      <td>3.600</td>
    </tr>
    <tr>
      <th>1</th>
      <td>A.D.P. Briggs/Leo Tolstoy/Fyodor Dostoyevsky</td>
      <td>3.760</td>
    </tr>
    <tr>
      <th>2</th>
      <td>A.E. Cunningham/Harlan Ellison/Charles F. Mill...</td>
      <td>4.150</td>
    </tr>
    <tr>
      <th>3</th>
      <td>A.J. Jacobs</td>
      <td>3.760</td>
    </tr>
    <tr>
      <th>4</th>
      <td>A.M. Homes</td>
      <td>3.280</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>6633</th>
      <td>Émile Zola/Henri Mitterand</td>
      <td>4.060</td>
    </tr>
    <tr>
      <th>6634</th>
      <td>Émile Zola/Robert Lethbridge/Elinor Dorday</td>
      <td>4.010</td>
    </tr>
    <tr>
      <th>6635</th>
      <td>Émile Zola/Robin Buss/Brian  Nelson</td>
      <td>3.990</td>
    </tr>
    <tr>
      <th>6636</th>
      <td>Émile Zola/Roger Pearson</td>
      <td>4.045</td>
    </tr>
    <tr>
      <th>6637</th>
      <td>Éric-Emmanuel Schmitt</td>
      <td>3.820</td>
    </tr>
  </tbody>
</table>
<p>6638 rows × 2 columns</p>
</div>




```python
plt.scatter(np.arange(len(mean_per_auth)), mean_per_auth['average_rating'], alpha=0.7)
plt.hlines(mean_per_auth['average_rating'].mean(), 0, len(mean_per_auth), colors='black')
plt.xlabel('')
plt.ylabel('Average Rating (Mean)')
plt.title('Average Rating x Authors')
plt.xticks(rotation=90);
```


    
![png](output_71_0.png)
    


Now let's go instead to analyze the distrubution of the num_of_pages feature



```python
plt.scatter(np.arange(len(data)), data['num_of_pages'], alpha=0.7)
plt.hlines(data['num_of_pages'].mean(), 0, len(data), colors='black')
plt.xlabel('Book')
plt.ylabel('Number of Pages')
plt.title('Number of pages distribution')
plt.xticks(rotation=90);
```


    
![png](output_73_0.png)
    


As we can see, there are values (called outliers) that are outside the normal distribution of the number of pages in a book. These can be called special cases that should be disregarded and also affect the parameters of the model.
If we take the mean and median as examples we can see that:


```python
data['num_of_pages'].mean(), data['num_of_pages'].median(), 
```




    (336.343943889938, 299.0)



If we go to exclude the top point, the value of the average even changes by one.


```python
data.drop(data.index[data['num_of_pages'].argmax()])['num_of_pages'].mean(), data.drop(data.index[data['num_of_pages'].argmax()])['num_of_pages'].median(), len(data)
```




    (335.7828237410072, 299.0, 11121)



_A bit of context_: This code performs the following operations:

- **data['num_of_pages'].argmax()** finds the index of the maximum value in the "num_of_pages" column of the DataFrame data. It identifies the row where the maximum number of pages is present.

- **data.index[data['num_of_pages'].argmax()]** retrieves the index label corresponding to the row with the maximum value in the "num_of_pages" column.

- **data.drop(data.index[data['num_of_pages'].argmax()])** drops the row with the maximum value in the "num_of_pages" column from the DataFrame data. The drop() function removes the specified index label from the DataFrame.

- **['num_of_pages'].mean()** calculates the mean of the remaining values in the "num_of_pages" column after removing the row with the maximum value. It computes the average number of pages.

- **['num_of_pages'].median()** calculates the median of the remaining values in the "num_of_pages" column after removing the row with the maximum value. It finds the middle value when the values are arranged in ascending order.

- **len(data)** returns the length of the DataFrame data, which represents the number of rows in the DataFrame.

Overall, the code removes the row with the maximum value in the "num_of_pages" column from the DataFrame data and then calculates the mean and median of the remaining values in that column. It also retrieves the total number of rows in the DataFrame.









A good way to go about removing outliers **in this case** is to approximate them to the 99th quantile


```python
mask = data['num_of_pages'] <= data['num_of_pages'].quantile(0.99)
mask
```




    0         True
    1         True
    2         True
    3         True
    4        False
             ...  
    11118     True
    11119     True
    11120     True
    11121     True
    11122     True
    Name: num_of_pages, Length: 11121, dtype: bool



_A bit of context_: The expression mask = data['num_of_pages'] <= data['num_of_pages'].quantile(0.99) creates a Boolean mask in the variable mask based on a condition applied to the "num_of_pages" column of the DataFrame data.

Let's break down the code and explain each part:

- __data['num_of_pages']__: This retrieves the values from the "num_of_pages" column of the DataFrame data. It represents the numerical values you want to compare.

- __.quantile(0.99)__: This calculates the 99th percentile of the values in the "num_of_pages" column. The quantile() method is used to determine the value below which 99% of the data falls.

- __data['num_of_pages'] <= data['num_of_pages'].quantile(0.99)__: This performs element-wise comparison between the values in the "num_of_pages" column and the 99th percentile value. It returns a Boolean Series with True for values that are less than or equal to the 99th percentile and False for values that are greater than the 99th percentile.

- __mask = data['num_of_pages'] <= data['num_of_pages'].quantile(0.99)__: This assigns the resulting Boolean Series to the variable mask. The mask will have the same length as the "num_of_pages" column, with True values corresponding to non-outliers (values less than or equal to the 99th percentile) and False values corresponding to outliers (values greater than the 99th percentile).

The purpose of this code is to create a mask that can be used to filter the DataFrame data and retain only the non-outlier values in the "num_of_pages" column.


```python
data[mask]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>title</th>
      <th>authors</th>
      <th>average_rating</th>
      <th>isbn</th>
      <th>isbn13</th>
      <th>language_code</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>publication_date</th>
      <th>publisher</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>2006-09-16</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>2004-09-01</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>J.K. Rowling</td>
      <td>4.42</td>
      <td>0439554896</td>
      <td>9780439554893</td>
      <td>eng</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>2003-11-01</td>
      <td>Scholastic</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5</td>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.56</td>
      <td>043965548X</td>
      <td>9780439655484</td>
      <td>eng</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>2004-05-01</td>
      <td>Scholastic Inc.</td>
    </tr>
    <tr>
      <th>5</th>
      <td>9</td>
      <td>Unauthorized Harry Potter Book Seven News: "Ha...</td>
      <td>W. Frederick Zimmerman</td>
      <td>3.74</td>
      <td>0976540606</td>
      <td>9780976540601</td>
      <td>en-US</td>
      <td>152</td>
      <td>19</td>
      <td>1</td>
      <td>2005-04-26</td>
      <td>Nimble Books</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>45631</td>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>William T. Vollmann/Larry McCaffery/Michael He...</td>
      <td>4.06</td>
      <td>1560254416</td>
      <td>9781560254416</td>
      <td>eng</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>2004-12-21</td>
      <td>Da Capo Press</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>45633</td>
      <td>You Bright and Risen Angels</td>
      <td>William T. Vollmann</td>
      <td>4.08</td>
      <td>0140110879</td>
      <td>9780140110876</td>
      <td>eng</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>1988-12-01</td>
      <td>Penguin Books</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>45634</td>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>William T. Vollmann</td>
      <td>3.96</td>
      <td>0140131965</td>
      <td>9780140131963</td>
      <td>eng</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>1993-08-01</td>
      <td>Penguin Books</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>45639</td>
      <td>Poor People</td>
      <td>William T. Vollmann</td>
      <td>3.72</td>
      <td>0060878827</td>
      <td>9780060878825</td>
      <td>eng</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>2007-02-27</td>
      <td>Ecco</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>45641</td>
      <td>Las aventuras de Tom Sawyer</td>
      <td>Mark Twain</td>
      <td>3.91</td>
      <td>8497646983</td>
      <td>9788497646987</td>
      <td>spa</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>2006-05-28</td>
      <td>Edimat Libros</td>
    </tr>
  </tbody>
</table>
<p>11009 rows × 12 columns</p>
</div>




```python
data[mask]['num_of_pages'].mean(), data[mask]['num_of_pages'].median(), 
```




    (323.7920792079208, 295.0)



As we can see the mean changed a lot with the application of the mask, in contrast the median changed by only 4 points. For this reason it is said that:
- **Median**: The median is a robust measure of central tendency because it is not affected by extreme values or outliers in the data. It represents the middle value when the data is sorted in ascending or descending order. Unlike the mean, it does not take into account the actual values of the data points but rather their relative position. Therefore, the median is less influenced by extreme values and provides a more robust estimate of the typical or central value of a dataset.

- **Mean**: The mean is a non-robust measure of central tendency because it is influenced by extreme values or outliers. It is calculated by summing all the values in the dataset and dividing by the total number of values. Since the mean incorporates the actual values of the data points, it can be strongly affected by outliers or skewed distributions. A single extreme value can have a significant impact on the mean, pulling it away from the central tendency of the majority of the data points.

In summary, the median is a robust measure of central tendency that is less affected by outliers, while the mean is sensitive to extreme values. Therefore, if the dataset contains outliers or is skewed, the median is often preferred as it provides a more representative estimate of the typical value. However, the choice between the median and mean depends on the specific characteristics of the data and the objectives of the analysis.

And if we re-plot our dataset with the mask applied, the distribution is almost perfect


```python
plt.scatter(np.arange(len(data[mask])), data[mask]['num_of_pages'], alpha=0.7)
plt.hlines(data[mask]['num_of_pages'].mean(), 0, len(data[mask]), colors='black')
plt.xlabel('')
plt.ylabel('Number of pages')
plt.title('Number of pages Distribution')
plt.xticks(rotation=90);
```


    
![png](output_86_0.png)
    


Using the .quantile(0.99) method can be a reasonable approach for removing outliers from a dataset. By calculating the 99th percentile of a variable, you can define a threshold above which values are considered outliers. Any value above this threshold can be considered an outlier and potentially removed from the dataset.

However, it's important to note that the appropriateness of using .quantile(0.99) as an outlier removal technique depends on the specific characteristics of your data and the context of your analysis. Here are a few considerations:

- _Understanding the data distribution_: Before applying the 99th percentile threshold, it's crucial to examine the distribution of the variable you're working with. If the data follows a normal distribution or another well-known distribution, using a fixed percentile like 99% can be effective. However, if the data has a skewed or heavy-tailed distribution, a fixed percentile might not capture all relevant outliers. In such cases, alternative techniques such as robust estimators or domain-specific knowledge may be more appropriate.

- _Domain knowledge_: Consider the specific domain and the nature of the data you are working with. Outliers can sometimes contain valuable information or represent genuine extreme events. Removing them without a thorough understanding of the underlying process may lead to biased or incomplete analysis. It's important to consult domain experts and consider the implications of removing outliers in your specific context.

- _Impact on the analysis_: Removing outliers can have an impact on subsequent analysis or modeling tasks. Depending on the dataset size and the extent of the outliers, removing them may introduce bias or affect the representativeness of the data. Carefully consider the potential consequences and evaluate how outlier removal affects the overall analysis objectives.

- _Sensitivity to the chosen threshold_: The choice of the 99th percentile as the outlier removal threshold is somewhat arbitrary. Modifying the threshold, such as using a more or less stringent value, can result in different outcomes. It's advisable to perform sensitivity analyses by trying different thresholds and evaluating their effects on the data and subsequent analyses.

In summary, while using .quantile(0.99) to remove outliers can be a useful technique, it should be applied with caution and in conjunction with other considerations such as data distribution, domain knowledge, and the impact on the analysis. Exploring alternative outlier detection methods and consulting with domain experts can provide a more comprehensive approach to identifying and handling outliers effectively.

## Features correlation

Find features correlation could be an important turning point to increase the performances of our model. But this is only one of the benefits, in fact finding the correlation between features is important for several reasons:

- **Feature Selection**: Correlation analysis helps identify the degree of association between features. Highly correlated features may provide redundant or overlapping information. In such cases, it is often beneficial to select a subset of features that are less correlated with each other to avoid multicollinearity. By removing redundant features, we can simplify the model, improve interpretability, and potentially enhance the model's performance.

- **Dimensionality Reduction**: Correlation analysis can aid in dimensionality reduction. By identifying features that have a weak or no correlation with the target variable, we can potentially eliminate those features without significantly impacting the model's predictive power. This can simplify the model and reduce computational complexity.

- **Interpretability**: Understanding the correlation between features can provide insights into the relationships and dependencies within the dataset. It helps in understanding the underlying structure of the data and identifying potential causal relationships. This knowledge can be crucial for domain experts and decision-making.

- **Feature Engineering**: Correlation analysis can guide feature engineering efforts. It can help identify feature combinations or interactions that have a high correlation with the target variable. Creating new features based on such correlations can potentially improve the model's performance and capture important patterns in the data.

- **Avoiding Spurious Relationships**: Correlation analysis helps identify spurious or misleading correlations. Just because two features are highly correlated does not necessarily imply a causal relationship between them. It is essential to exercise caution and apply domain knowledge to avoid drawing incorrect conclusions based solely on correlations.

- **Model Performance**: Correlated features can adversely impact model performance, especially in models that assume feature independence. Highly correlated features can lead to multicollinearity, which can cause instability in coefficient estimates, reduce model interpretability, and increase the risk of overfitting.

There are several way to calculate the feature correlation:
- _Pearson Correlation Coefficient_: The Pearson correlation coefficient is a widely used measure to quantify the linear relationship between two continuous variables. It measures the strength and direction of the linear association between variables, ranging from -1 (perfect negative correlation) to 1 (perfect positive correlation). A value of 0 indicates no linear correlation. The corr() function in libraries like Pandas or NumPy can be used to calculate the Pearson correlation coefficient.

- _Spearman Rank Correlation_: The Spearman rank correlation is a non-parametric measure that assesses the monotonic relationship between two variables. It is useful when the variables are not necessarily linearly related but still exhibit a consistent pattern. Spearman correlation calculates the correlation based on the ranks of the data rather than the actual values. It can be computed using the corr() function with the parameter method='spearman'.

- _Kendall Rank Correlation_: The Kendall rank correlation is another non-parametric method used to measure the ordinal association between two variables. It determines the similarity of the rank orders of the data. Like Spearman correlation, Kendall correlation also ranges from -1 to 1, with 0 indicating no correlation. The corr() function with method='kendall' can be used to calculate Kendall correlation.

- _Heatmap_: A heatmap is a graphical representation that provides an overview of the correlation between multiple variables in a dataset. It uses colors to indicate the strength and direction of the correlation. Heatmaps can be created using libraries like Seaborn or Matplotlib.

- _Scatter Plots_: Scatter plots are useful for visualizing the relationship between two variables. They can provide insights into the type of correlation (positive, negative, or none) by plotting the variables' values on a Cartesian plane.

- _Pairwise Correlation Matrix_: Computing and visualizing the pairwise correlation matrix can help identify correlations among multiple variables simultaneously. The correlation matrix is a square matrix where each element represents the correlation between two variables. Heatmaps or correlation matrix plots can be used to visualize the matrix.

- _Feature Importance Techniques_: Some machine learning algorithms, such as decision trees or ensemble methods like Random Forest and Gradient Boosting, can provide an estimate of feature importance. These techniques can help identify the relative importance of different features in predicting the target variable, indirectly revealing correlations

#### Number of pages and average rating correlations

Analyzing the dataset, one could try to evaluate a possible correlation between num_of_pages feature and average rating, let's try.


```python
plt.scatter(data['num_of_pages'], data['average_rating'], alpha=0.7)
plt.xlabel('Number of pages')
plt.ylabel('Average Rating')
plt.title('Page x rating correlation')
plt.xticks(rotation=90);
```


    
![png](output_93_0.png)
    


There does not appear to be an actual correlation



```python
column1_column2_corr = data['num_of_pages'].corr(data['average_rating'], method='pearson')
column1_column2_corr
```




    0.15043008473440947




```python
column1_column2_corr = data['num_of_pages'].corr(data['average_rating'], method='spearman')
column1_column2_corr
```




    0.10980981543504043



#### Text review and average rating correlations


```python
plt.scatter(data['text_reviews_count'], data['average_rating'], alpha=0.7)
plt.xlabel('Review Count')
plt.ylabel('Average Rating')
plt.title('Page x number of reviews')
plt.xticks(rotation=90);
```


    
![png](output_98_0.png)
    



```python
column1_column2_corr = data['text_reviews_count'].corr(data['average_rating'], method='pearson')
column1_column2_corr
```




    0.03366883758266891




```python
column1_column2_corr = data['text_reviews_count'].corr(data['average_rating'], method='spearman')
column1_column2_corr
```




    0.032072720814460914



#### Text review and number of pages correlations


```python
plt.scatter(data['num_of_pages'], data['text_reviews_count'], alpha=0.7)
plt.ylabel('Review Count')
plt.xlabel('Number of pages')
plt.title('Page x number of reviews')
plt.xticks(rotation=90);
```


    
![png](output_102_0.png)
    



```python
column1_column2_corr = data['text_reviews_count'].corr(data['num_of_pages'], method='pearson')
column1_column2_corr
```




    0.0370207814936928




```python
column1_column2_corr = data['text_reviews_count'].corr(data['num_of_pages'], method='spearman')
column1_column2_corr
```




    0.1681460062694404




```python

```


```python

```

# PASSAGGI DA SPIEGARE MEGLIO

##### Generate a new column to split in bins


```python
def segregate(data):
    values = []
    for val in data.average_rating:
        if val >= 0 and val <= 1:
            values.append("Between 0 and 1")
        elif val > 1 and val <= 2:
            values.append("Between 1 and 2")
        elif val > 2 and val <= 3:
            values.append("Between 2 and 3")
        elif val > 3 and val <= 4:
            values.append("Between 3 and 4")
        elif val > 4 and val <= 5:
            values.append("Between 4 and 5")
        else:
            values.append("NaN")
    return values
```


```python
data['bin'] = pd.cut(data['average_rating'], range(6))
data
```

    /var/folders/cq/dplqsscx73v3bv7s5nkwr5fc0000gn/T/ipykernel_1122/1279254373.py:1: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      data['bin'] = pd.cut(data['average_rating'], range(6))





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>title</th>
      <th>authors</th>
      <th>average_rating</th>
      <th>isbn</th>
      <th>isbn13</th>
      <th>language_code</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>publication_date</th>
      <th>publisher</th>
      <th>bin</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>2006-09-16</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>2004-09-01</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>J.K. Rowling</td>
      <td>4.42</td>
      <td>0439554896</td>
      <td>9780439554893</td>
      <td>eng</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>2003-11-01</td>
      <td>Scholastic</td>
      <td>(4, 5]</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5</td>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.56</td>
      <td>043965548X</td>
      <td>9780439655484</td>
      <td>eng</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>2004-05-01</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.78</td>
      <td>0439682584</td>
      <td>9780439682589</td>
      <td>eng</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>2004-09-13</td>
      <td>Scholastic</td>
      <td>(4, 5]</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>45631</td>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>William T. Vollmann/Larry McCaffery/Michael He...</td>
      <td>4.06</td>
      <td>1560254416</td>
      <td>9781560254416</td>
      <td>eng</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>2004-12-21</td>
      <td>Da Capo Press</td>
      <td>(4, 5]</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>45633</td>
      <td>You Bright and Risen Angels</td>
      <td>William T. Vollmann</td>
      <td>4.08</td>
      <td>0140110879</td>
      <td>9780140110876</td>
      <td>eng</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>1988-12-01</td>
      <td>Penguin Books</td>
      <td>(4, 5]</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>45634</td>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>William T. Vollmann</td>
      <td>3.96</td>
      <td>0140131965</td>
      <td>9780140131963</td>
      <td>eng</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>1993-08-01</td>
      <td>Penguin Books</td>
      <td>(3, 4]</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>45639</td>
      <td>Poor People</td>
      <td>William T. Vollmann</td>
      <td>3.72</td>
      <td>0060878827</td>
      <td>9780060878825</td>
      <td>eng</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>2007-02-27</td>
      <td>Ecco</td>
      <td>(3, 4]</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>45641</td>
      <td>Las aventuras de Tom Sawyer</td>
      <td>Mark Twain</td>
      <td>3.91</td>
      <td>8497646983</td>
      <td>9788497646987</td>
      <td>spa</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>2006-05-28</td>
      <td>Edimat Libros</td>
      <td>(3, 4]</td>
    </tr>
  </tbody>
</table>
<p>11121 rows × 13 columns</p>
</div>




```python
data['ratings_dist'] = segregate(data)
data
```

    /var/folders/cq/dplqsscx73v3bv7s5nkwr5fc0000gn/T/ipykernel_1122/3749760970.py:1: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      data['ratings_dist'] = segregate(data)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>title</th>
      <th>authors</th>
      <th>average_rating</th>
      <th>isbn</th>
      <th>isbn13</th>
      <th>language_code</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>publication_date</th>
      <th>publisher</th>
      <th>bin</th>
      <th>ratings_dist</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>2006-09-16</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>2004-09-01</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>J.K. Rowling</td>
      <td>4.42</td>
      <td>0439554896</td>
      <td>9780439554893</td>
      <td>eng</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>2003-11-01</td>
      <td>Scholastic</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5</td>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.56</td>
      <td>043965548X</td>
      <td>9780439655484</td>
      <td>eng</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>2004-05-01</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.78</td>
      <td>0439682584</td>
      <td>9780439682589</td>
      <td>eng</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>2004-09-13</td>
      <td>Scholastic</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>45631</td>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>William T. Vollmann/Larry McCaffery/Michael He...</td>
      <td>4.06</td>
      <td>1560254416</td>
      <td>9781560254416</td>
      <td>eng</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>2004-12-21</td>
      <td>Da Capo Press</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>45633</td>
      <td>You Bright and Risen Angels</td>
      <td>William T. Vollmann</td>
      <td>4.08</td>
      <td>0140110879</td>
      <td>9780140110876</td>
      <td>eng</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>1988-12-01</td>
      <td>Penguin Books</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>45634</td>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>William T. Vollmann</td>
      <td>3.96</td>
      <td>0140131965</td>
      <td>9780140131963</td>
      <td>eng</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>1993-08-01</td>
      <td>Penguin Books</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>45639</td>
      <td>Poor People</td>
      <td>William T. Vollmann</td>
      <td>3.72</td>
      <td>0060878827</td>
      <td>9780060878825</td>
      <td>eng</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>2007-02-27</td>
      <td>Ecco</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>45641</td>
      <td>Las aventuras de Tom Sawyer</td>
      <td>Mark Twain</td>
      <td>3.91</td>
      <td>8497646983</td>
      <td>9788497646987</td>
      <td>spa</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>2006-05-28</td>
      <td>Edimat Libros</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
  </tbody>
</table>
<p>11121 rows × 14 columns</p>
</div>




```python
data['ratings_dist'] = segregate(data)
data
```

    /var/folders/cq/dplqsscx73v3bv7s5nkwr5fc0000gn/T/ipykernel_1122/3749760970.py:1: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      data['ratings_dist'] = segregate(data)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>title</th>
      <th>authors</th>
      <th>average_rating</th>
      <th>isbn</th>
      <th>isbn13</th>
      <th>language_code</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>publication_date</th>
      <th>publisher</th>
      <th>bin</th>
      <th>ratings_dist</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>2006-09-16</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>2004-09-01</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>J.K. Rowling</td>
      <td>4.42</td>
      <td>0439554896</td>
      <td>9780439554893</td>
      <td>eng</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>2003-11-01</td>
      <td>Scholastic</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5</td>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.56</td>
      <td>043965548X</td>
      <td>9780439655484</td>
      <td>eng</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>2004-05-01</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.78</td>
      <td>0439682584</td>
      <td>9780439682589</td>
      <td>eng</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>2004-09-13</td>
      <td>Scholastic</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>45631</td>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>William T. Vollmann/Larry McCaffery/Michael He...</td>
      <td>4.06</td>
      <td>1560254416</td>
      <td>9781560254416</td>
      <td>eng</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>2004-12-21</td>
      <td>Da Capo Press</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>45633</td>
      <td>You Bright and Risen Angels</td>
      <td>William T. Vollmann</td>
      <td>4.08</td>
      <td>0140110879</td>
      <td>9780140110876</td>
      <td>eng</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>1988-12-01</td>
      <td>Penguin Books</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>45634</td>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>William T. Vollmann</td>
      <td>3.96</td>
      <td>0140131965</td>
      <td>9780140131963</td>
      <td>eng</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>1993-08-01</td>
      <td>Penguin Books</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>45639</td>
      <td>Poor People</td>
      <td>William T. Vollmann</td>
      <td>3.72</td>
      <td>0060878827</td>
      <td>9780060878825</td>
      <td>eng</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>2007-02-27</td>
      <td>Ecco</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>45641</td>
      <td>Las aventuras de Tom Sawyer</td>
      <td>Mark Twain</td>
      <td>3.91</td>
      <td>8497646983</td>
      <td>9788497646987</td>
      <td>spa</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>2006-05-28</td>
      <td>Edimat Libros</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
  </tbody>
</table>
<p>11121 rows × 14 columns</p>
</div>




```python
model_data = data[['title','average_rating','num_of_pages','ratings_count','text_reviews_count','ratings_dist']].copy()
```


```python
model_data['ratings_dist'] = model_data['ratings_dist'].replace({
    'Between 0 and 1':0,
    'Between 1 and 2':1,
    'Between 2 and 3':2,
    'Between 3 and 4':3,
    'Between 4 and 5':4
});
data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>title</th>
      <th>authors</th>
      <th>average_rating</th>
      <th>isbn</th>
      <th>isbn13</th>
      <th>language_code</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>publication_date</th>
      <th>publisher</th>
      <th>bin</th>
      <th>ratings_dist</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>2006-09-16</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>2004-09-01</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>J.K. Rowling</td>
      <td>4.42</td>
      <td>0439554896</td>
      <td>9780439554893</td>
      <td>eng</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>2003-11-01</td>
      <td>Scholastic</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5</td>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.56</td>
      <td>043965548X</td>
      <td>9780439655484</td>
      <td>eng</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>2004-05-01</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.78</td>
      <td>0439682584</td>
      <td>9780439682589</td>
      <td>eng</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>2004-09-13</td>
      <td>Scholastic</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>45631</td>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>William T. Vollmann/Larry McCaffery/Michael He...</td>
      <td>4.06</td>
      <td>1560254416</td>
      <td>9781560254416</td>
      <td>eng</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>2004-12-21</td>
      <td>Da Capo Press</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>45633</td>
      <td>You Bright and Risen Angels</td>
      <td>William T. Vollmann</td>
      <td>4.08</td>
      <td>0140110879</td>
      <td>9780140110876</td>
      <td>eng</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>1988-12-01</td>
      <td>Penguin Books</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>45634</td>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>William T. Vollmann</td>
      <td>3.96</td>
      <td>0140131965</td>
      <td>9780140131963</td>
      <td>eng</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>1993-08-01</td>
      <td>Penguin Books</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>45639</td>
      <td>Poor People</td>
      <td>William T. Vollmann</td>
      <td>3.72</td>
      <td>0060878827</td>
      <td>9780060878825</td>
      <td>eng</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>2007-02-27</td>
      <td>Ecco</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>45641</td>
      <td>Las aventuras de Tom Sawyer</td>
      <td>Mark Twain</td>
      <td>3.91</td>
      <td>8497646983</td>
      <td>9788497646987</td>
      <td>spa</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>2006-05-28</td>
      <td>Edimat Libros</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
  </tbody>
</table>
<p>11121 rows × 14 columns</p>
</div>




```python
model_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>title</th>
      <th>average_rating</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>ratings_dist</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>4.57</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>4</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>4.49</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>4.42</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>4</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>4.56</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>4.78</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>4</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>4.06</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>4</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>You Bright and Risen Angels</td>
      <td>4.08</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>4</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>3.96</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>3</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>Poor People</td>
      <td>3.72</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>3</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>Las aventuras de Tom Sawyer</td>
      <td>3.91</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
<p>11121 rows × 6 columns</p>
</div>




```python

```


```python

```


```python

```


```python

```

## Dimensionality Reduction


#### PCA (Principal Component Analysis)

PCA (Principal Component Analysis) is a dimensionality reduction technique commonly used in machine learning and data analysis. It is used to transform a high-dimensional dataset into a lower-dimensional space while retaining as much information as possible.

The main goal of PCA is to find a new set of orthogonal variables, called principal components, that capture the maximum variance in the original dataset. These principal components are linear combinations of the original variables. The first principal component captures the most significant variance, the second captures the second most significant variance, and so on.

PCA works by identifying the directions in the original feature space along which the data varies the most. It then projects the data onto these directions to create the new feature space. By reducing the dimensionality of the data, PCA can simplify the analysis, visualize the data in a lower-dimensional space, and improve the performance of machine learning algorithms by removing noise and redundant information.

The steps involved in performing PCA are as follows:

1 - Standardize the data: If the variables in the dataset have different scales, it is important to standardize them to have zero mean and unit variance.

2 - Compute the covariance matrix: Calculate the covariance matrix of the standardized data, which represents the relationships between the variables.

3 - Compute the eigenvectors and eigenvalues: Find the eigenvectors and eigenvalues of the covariance matrix. The eigenvectors represent the principal components, and the corresponding eigenvalues indicate the amount of variance captured by each component.

4 - Select the principal components: Sort the eigenvalues in descending order and select the top k eigenvectors that correspond to the largest eigenvalues to retain the most significant information.

5 - Project the data: Transform the original data by projecting it onto the selected principal components to create the new lower-dimensional feature space.


```python
import pandas
from sklearn.decomposition import PCA, IncrementalPCA
import numpy
import matplotlib.pyplot as plot
```


```python
pca_data = model_data[['average_rating','num_of_pages','ratings_count','text_reviews_count','ratings_dist']].copy()
df_normalized = (pca_data - pca_data.mean()) / pca_data.std()

pca = IncrementalPCA(n_components=pca_data.shape[1])
```

_a bit of context_:
- __pca_data = model_data[['average_rating','num_of_pages','ratings_count','text_reviews_count','ratings_dist']].copy()__: This line selects the desired columns from the model_data DataFrame, just like before.
- The following code perform normalization for each column in pca_data individually:
```python
    df_normalized=(pca_data - pca_data.mean()) / pca_data.std()
```
For each column, the corresponding column in df_normalized is computed by subtracting the mean of the column from pca_data and then dividing it by the standard deviation of that column. This process ensures that each column is standardized independently.

- __pca = IncrementalPCA(n_components=pca_data.shape[1])__: This line remains the same as before, creating an instance of the IncrementalPCA class with the number of components set to the number of columns in pca_data.

By normalizing each column individually, you can ensure that the data is standardized appropriately, taking into account the specific characteristics of each feature. This is particularly useful when the columns have different scales or units. After normalization, the PCA model can be fitted to the normalized data to compute the principal components.







```python
%%time
pca.fit(df_normalized)
```

    CPU times: user 28.7 ms, sys: 1.85 ms, total: 30.5 ms
    Wall time: 31.9 ms





<style>#sk-container-id-1 {color: black;background-color: white;}#sk-container-id-1 pre{padding: 0;}#sk-container-id-1 div.sk-toggleable {background-color: white;}#sk-container-id-1 label.sk-toggleable__label {cursor: pointer;display: block;width: 100%;margin-bottom: 0;padding: 0.3em;box-sizing: border-box;text-align: center;}#sk-container-id-1 label.sk-toggleable__label-arrow:before {content: "▸";float: left;margin-right: 0.25em;color: #696969;}#sk-container-id-1 label.sk-toggleable__label-arrow:hover:before {color: black;}#sk-container-id-1 div.sk-estimator:hover label.sk-toggleable__label-arrow:before {color: black;}#sk-container-id-1 div.sk-toggleable__content {max-height: 0;max-width: 0;overflow: hidden;text-align: left;background-color: #f0f8ff;}#sk-container-id-1 div.sk-toggleable__content pre {margin: 0.2em;color: black;border-radius: 0.25em;background-color: #f0f8ff;}#sk-container-id-1 input.sk-toggleable__control:checked~div.sk-toggleable__content {max-height: 200px;max-width: 100%;overflow: auto;}#sk-container-id-1 input.sk-toggleable__control:checked~label.sk-toggleable__label-arrow:before {content: "▾";}#sk-container-id-1 div.sk-estimator input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-1 div.sk-label input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-1 input.sk-hidden--visually {border: 0;clip: rect(1px 1px 1px 1px);clip: rect(1px, 1px, 1px, 1px);height: 1px;margin: -1px;overflow: hidden;padding: 0;position: absolute;width: 1px;}#sk-container-id-1 div.sk-estimator {font-family: monospace;background-color: #f0f8ff;border: 1px dotted black;border-radius: 0.25em;box-sizing: border-box;margin-bottom: 0.5em;}#sk-container-id-1 div.sk-estimator:hover {background-color: #d4ebff;}#sk-container-id-1 div.sk-parallel-item::after {content: "";width: 100%;border-bottom: 1px solid gray;flex-grow: 1;}#sk-container-id-1 div.sk-label:hover label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-1 div.sk-serial::before {content: "";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: 0;}#sk-container-id-1 div.sk-serial {display: flex;flex-direction: column;align-items: center;background-color: white;padding-right: 0.2em;padding-left: 0.2em;position: relative;}#sk-container-id-1 div.sk-item {position: relative;z-index: 1;}#sk-container-id-1 div.sk-parallel {display: flex;align-items: stretch;justify-content: center;background-color: white;position: relative;}#sk-container-id-1 div.sk-item::before, #sk-container-id-1 div.sk-parallel-item::before {content: "";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: -1;}#sk-container-id-1 div.sk-parallel-item {display: flex;flex-direction: column;z-index: 1;position: relative;background-color: white;}#sk-container-id-1 div.sk-parallel-item:first-child::after {align-self: flex-end;width: 50%;}#sk-container-id-1 div.sk-parallel-item:last-child::after {align-self: flex-start;width: 50%;}#sk-container-id-1 div.sk-parallel-item:only-child::after {width: 0;}#sk-container-id-1 div.sk-dashed-wrapped {border: 1px dashed gray;margin: 0 0.4em 0.5em 0.4em;box-sizing: border-box;padding-bottom: 0.4em;background-color: white;}#sk-container-id-1 div.sk-label label {font-family: monospace;font-weight: bold;display: inline-block;line-height: 1.2em;}#sk-container-id-1 div.sk-label-container {text-align: center;}#sk-container-id-1 div.sk-container {/* jupyter's `normalize.less` sets `[hidden] { display: none; }` but bootstrap.min.css set `[hidden] { display: none !important; }` so we also need the `!important` here to be able to override the default hidden behavior on the sphinx rendered scikit-learn.org. See: https://github.com/scikit-learn/scikit-learn/issues/21755 */display: inline-block !important;position: relative;}#sk-container-id-1 div.sk-text-repr-fallback {display: none;}</style><div id="sk-container-id-1" class="sk-top-container"><div class="sk-text-repr-fallback"><pre>IncrementalPCA(n_components=5)</pre><b>In a Jupyter environment, please rerun this cell to show the HTML representation or trust the notebook. <br />On GitHub, the HTML representation is unable to render, please try loading this page with nbviewer.org.</b></div><div class="sk-container" hidden><div class="sk-item"><div class="sk-estimator sk-toggleable"><input class="sk-toggleable__control sk-hidden--visually" id="sk-estimator-id-1" type="checkbox" checked><label for="sk-estimator-id-1" class="sk-toggleable__label sk-toggleable__label-arrow">IncrementalPCA</label><div class="sk-toggleable__content"><pre>IncrementalPCA(n_components=5)</pre></div></div></div></div></div>



The fit() method in PCA analyzes the input dataset df_normalized to learn and extract important features, called principal components, from the data. Before applying PCA, it's usually recommended to normalize or standardize the dataset to ensure that all features have similar scales. In this case, df_normalized is the preprocessed dataset.

During the fitting process, PCA calculates the principal components by finding linear combinations of the original features that capture the maximum amount of variance in the data. The principal components are orthogonal to each other, which means they are uncorrelated. The first principal component captures the most significant variance, followed by the second, third, and so on.

After the fit() method is executed, the pca object will contain the learned information about the principal components. This information can then be used for various purposes, such as transforming new data to the PCA space or analyzing the explained variance ratios of the principal components.

#### Fit optimization (~3 or 4 times faster)


```python
%%time
for chunk in np.array_split(pca_data, 5):
    pca.partial_fit(chunk)
pca
```

    CPU times: user 3.42 ms, sys: 545 µs, total: 3.97 ms
    Wall time: 4.77 ms





<style>#sk-container-id-2 {color: black;background-color: white;}#sk-container-id-2 pre{padding: 0;}#sk-container-id-2 div.sk-toggleable {background-color: white;}#sk-container-id-2 label.sk-toggleable__label {cursor: pointer;display: block;width: 100%;margin-bottom: 0;padding: 0.3em;box-sizing: border-box;text-align: center;}#sk-container-id-2 label.sk-toggleable__label-arrow:before {content: "▸";float: left;margin-right: 0.25em;color: #696969;}#sk-container-id-2 label.sk-toggleable__label-arrow:hover:before {color: black;}#sk-container-id-2 div.sk-estimator:hover label.sk-toggleable__label-arrow:before {color: black;}#sk-container-id-2 div.sk-toggleable__content {max-height: 0;max-width: 0;overflow: hidden;text-align: left;background-color: #f0f8ff;}#sk-container-id-2 div.sk-toggleable__content pre {margin: 0.2em;color: black;border-radius: 0.25em;background-color: #f0f8ff;}#sk-container-id-2 input.sk-toggleable__control:checked~div.sk-toggleable__content {max-height: 200px;max-width: 100%;overflow: auto;}#sk-container-id-2 input.sk-toggleable__control:checked~label.sk-toggleable__label-arrow:before {content: "▾";}#sk-container-id-2 div.sk-estimator input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-2 div.sk-label input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-2 input.sk-hidden--visually {border: 0;clip: rect(1px 1px 1px 1px);clip: rect(1px, 1px, 1px, 1px);height: 1px;margin: -1px;overflow: hidden;padding: 0;position: absolute;width: 1px;}#sk-container-id-2 div.sk-estimator {font-family: monospace;background-color: #f0f8ff;border: 1px dotted black;border-radius: 0.25em;box-sizing: border-box;margin-bottom: 0.5em;}#sk-container-id-2 div.sk-estimator:hover {background-color: #d4ebff;}#sk-container-id-2 div.sk-parallel-item::after {content: "";width: 100%;border-bottom: 1px solid gray;flex-grow: 1;}#sk-container-id-2 div.sk-label:hover label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-2 div.sk-serial::before {content: "";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: 0;}#sk-container-id-2 div.sk-serial {display: flex;flex-direction: column;align-items: center;background-color: white;padding-right: 0.2em;padding-left: 0.2em;position: relative;}#sk-container-id-2 div.sk-item {position: relative;z-index: 1;}#sk-container-id-2 div.sk-parallel {display: flex;align-items: stretch;justify-content: center;background-color: white;position: relative;}#sk-container-id-2 div.sk-item::before, #sk-container-id-2 div.sk-parallel-item::before {content: "";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: -1;}#sk-container-id-2 div.sk-parallel-item {display: flex;flex-direction: column;z-index: 1;position: relative;background-color: white;}#sk-container-id-2 div.sk-parallel-item:first-child::after {align-self: flex-end;width: 50%;}#sk-container-id-2 div.sk-parallel-item:last-child::after {align-self: flex-start;width: 50%;}#sk-container-id-2 div.sk-parallel-item:only-child::after {width: 0;}#sk-container-id-2 div.sk-dashed-wrapped {border: 1px dashed gray;margin: 0 0.4em 0.5em 0.4em;box-sizing: border-box;padding-bottom: 0.4em;background-color: white;}#sk-container-id-2 div.sk-label label {font-family: monospace;font-weight: bold;display: inline-block;line-height: 1.2em;}#sk-container-id-2 div.sk-label-container {text-align: center;}#sk-container-id-2 div.sk-container {/* jupyter's `normalize.less` sets `[hidden] { display: none; }` but bootstrap.min.css set `[hidden] { display: none !important; }` so we also need the `!important` here to be able to override the default hidden behavior on the sphinx rendered scikit-learn.org. See: https://github.com/scikit-learn/scikit-learn/issues/21755 */display: inline-block !important;position: relative;}#sk-container-id-2 div.sk-text-repr-fallback {display: none;}</style><div id="sk-container-id-2" class="sk-top-container"><div class="sk-text-repr-fallback"><pre>IncrementalPCA(n_components=5)</pre><b>In a Jupyter environment, please rerun this cell to show the HTML representation or trust the notebook. <br />On GitHub, the HTML representation is unable to render, please try loading this page with nbviewer.org.</b></div><div class="sk-container" hidden><div class="sk-item"><div class="sk-estimator sk-toggleable"><input class="sk-toggleable__control sk-hidden--visually" id="sk-estimator-id-2" type="checkbox" checked><label for="sk-estimator-id-2" class="sk-toggleable__label sk-toggleable__label-arrow">IncrementalPCA</label><div class="sk-toggleable__content"><pre>IncrementalPCA(n_components=5)</pre></div></div></div></div></div>



_a bit of context_:
- np.array_split(pca_data, 5) splits the dataset pca_data into five equal-sized chunks. This function is provided by the NumPy library and divides the data along the first dimension.

- The code then enters a loop where each chunk of the dataset is processed.

- pca.partial_fit(chunk) performs a partial fit of the current chunk on the PCA object pca. Instead of fitting the entire dataset at once, partial_fit() allows for incremental updates to the PCA model. This is useful when dealing with large datasets that may not fit into memory or when the data arrives in chunks or streams.

- Each chunk is sequentially passed through the partial_fit() method, updating the PCA model incrementally. By doing so, the model gradually adapts to the statistical properties of the data, allowing for an approximation of the principal components without needing the entire dataset in memory.

In summary, the code performs incremental PCA by splitting the dataset into smaller chunks and applying partial_fit() on each chunk successively. This approach is useful for handling large datasets or situations where the data is received or processed incrementally.


```python
df_transformed = pca.transform(df_normalized)
df_transformed.shape
```




    (11121, 5)



```df_transformed = pca.transform(df_normalized)``` applies the PCA transformation to the normalized dataset, and df_transformed.shape retrieves the shape of the transformed dataset, providing information about the number of samples and the number of principal components.


```python
pca = PCA(n_components=2)
t = pca.fit_transform(df_normalized)
t, t.shape
```




    (array([[16.90306934, 11.82172235],
            [17.56686355, 12.3406451 ],
            [ 1.01629667, -1.40456775],
            ...,
            [-0.42525128,  0.15973546],
            [-0.71222276,  0.51182842],
            [-0.61585373,  0.30392888]]),
     (11121, 2))



- pca = PCA(n_components=2) creates an instance of the PCA class with the argument n_components set to 2. This specifies that we want to reduce the dimensionality of the dataset to two principal components.

- t = pca.fit_transform(df_normalized) fits the PCA model to the normalized dataset df_normalized and transforms it into a new dataset t using the learned principal components. The fit_transform() method combines the process of fitting the PCA model to the data (fit()) and transforming the data into the reduced dimensionality space (transform()).

- t contains the transformed dataset where each sample is represented by two principal components. The number of columns or features in t is equal to the number of principal components specified in n_components, which in this case is 2.

- t.shape retrieves the shape or dimensions of the transformed dataset t. It returns a tuple (n_samples, n_components) that represents the number of samples or instances in the transformed dataset (n_samples) and the number of principal components (n_components).


```python
plot.scatter(t[:,0],t[:,1],)
plot.show()
```


    
![png](output_136_0.png)
    


#### T-SNE(t-Distributed Stochastic Neighbor Embedding)

t-SNE (t-Distributed Stochastic Neighbor Embedding) is a dimensionality reduction technique commonly used for visualizing high-dimensional data in a lower-dimensional space. It is particularly effective at capturing the complex and nonlinear relationships present in the data.

The main goal of t-SNE is to map high-dimensional data points to a lower-dimensional space while preserving the local and global structure of the data as much as possible. It achieves this by modeling pairwise similarities between data points in both the high-dimensional and low-dimensional spaces.

The algorithm starts by calculating the similarity or dissimilarity between each pair of high-dimensional data points. It uses a Gaussian kernel or a student's t-distribution to measure the similarity, giving more weight to nearby points and less weight to distant points.

Next, t-SNE constructs a probability distribution over pairs of high-dimensional data points, aiming to maintain the similarities observed in the previous step. It also constructs a similar probability distribution over pairs of low-dimensional (typically 2D) points.

The algorithm then minimizes the Kullback-Leibler divergence between the two distributions, adjusting the positions of the low-dimensional points iteratively. This optimization process allows t-SNE to find an embedding where nearby points in the high-dimensional space are represented as nearby points in the low-dimensional space.

One important characteristic of t-SNE is that it tends to emphasize the separation between different clusters or groups of data points. This makes it useful for visualizing and exploring complex datasets, identifying patterns, and discovering underlying structures.

However, it's worth noting that t-SNE is computationally intensive and may not scale well to very large datasets. Additionally, the results of t-SNE can vary depending on the random initialization and the choice of hyperparameters.

In summary, t-SNE is a dimensionality reduction technique that preserves local and global structures in high-dimensional data, allowing for effective visualization and analysis of complex datasets.


```python
from sklearn.manifold import TSNE
```


```python
tsne = TSNE(n_components=2)
```


```python
%%time
t = tsne.fit_transform(df_normalized)
t, t.shape
```

    CPU times: user 2min 22s, sys: 25.8 s, total: 2min 48s
    Wall time: 22.1 s





    (array([[ 55.304363,  22.68074 ],
            [ 55.41703 ,  22.723616],
            [ 53.94872 , -60.091248],
            ...,
            [ 25.605017,  15.72334 ],
            [ 13.674971,  63.071236],
            [-23.946262,  -9.009914]], dtype=float32),
     (11121, 2))




```python
plot.scatter(t[:,0],t[:,1],c=df_normalized.ratings_dist)
plot.show()
```


    
![png](output_142_0.png)
    


# Model Training

#### Recomender system with k-neighbors

Now we will go on to create some model examples. We will start by using a NearestNeighbors to get the recommended books returned given a newly read book


```python
from sklearn.neighbors import NearestNeighbors
import warnings
warnings.filterwarnings("ignore")
```


```python
# Creating an instance of the NearestNeighbors model
model = NearestNeighbors(n_neighbors=6, algorithm='ball_tree')
```


```python
# Fitting the model to the feature matrix of books
model.fit(model_data[['average_rating','num_of_pages','ratings_count','text_reviews_count','ratings_dist']])
```




<style>#sk-container-id-3 {color: black;background-color: white;}#sk-container-id-3 pre{padding: 0;}#sk-container-id-3 div.sk-toggleable {background-color: white;}#sk-container-id-3 label.sk-toggleable__label {cursor: pointer;display: block;width: 100%;margin-bottom: 0;padding: 0.3em;box-sizing: border-box;text-align: center;}#sk-container-id-3 label.sk-toggleable__label-arrow:before {content: "▸";float: left;margin-right: 0.25em;color: #696969;}#sk-container-id-3 label.sk-toggleable__label-arrow:hover:before {color: black;}#sk-container-id-3 div.sk-estimator:hover label.sk-toggleable__label-arrow:before {color: black;}#sk-container-id-3 div.sk-toggleable__content {max-height: 0;max-width: 0;overflow: hidden;text-align: left;background-color: #f0f8ff;}#sk-container-id-3 div.sk-toggleable__content pre {margin: 0.2em;color: black;border-radius: 0.25em;background-color: #f0f8ff;}#sk-container-id-3 input.sk-toggleable__control:checked~div.sk-toggleable__content {max-height: 200px;max-width: 100%;overflow: auto;}#sk-container-id-3 input.sk-toggleable__control:checked~label.sk-toggleable__label-arrow:before {content: "▾";}#sk-container-id-3 div.sk-estimator input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-3 div.sk-label input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-3 input.sk-hidden--visually {border: 0;clip: rect(1px 1px 1px 1px);clip: rect(1px, 1px, 1px, 1px);height: 1px;margin: -1px;overflow: hidden;padding: 0;position: absolute;width: 1px;}#sk-container-id-3 div.sk-estimator {font-family: monospace;background-color: #f0f8ff;border: 1px dotted black;border-radius: 0.25em;box-sizing: border-box;margin-bottom: 0.5em;}#sk-container-id-3 div.sk-estimator:hover {background-color: #d4ebff;}#sk-container-id-3 div.sk-parallel-item::after {content: "";width: 100%;border-bottom: 1px solid gray;flex-grow: 1;}#sk-container-id-3 div.sk-label:hover label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-3 div.sk-serial::before {content: "";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: 0;}#sk-container-id-3 div.sk-serial {display: flex;flex-direction: column;align-items: center;background-color: white;padding-right: 0.2em;padding-left: 0.2em;position: relative;}#sk-container-id-3 div.sk-item {position: relative;z-index: 1;}#sk-container-id-3 div.sk-parallel {display: flex;align-items: stretch;justify-content: center;background-color: white;position: relative;}#sk-container-id-3 div.sk-item::before, #sk-container-id-3 div.sk-parallel-item::before {content: "";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: -1;}#sk-container-id-3 div.sk-parallel-item {display: flex;flex-direction: column;z-index: 1;position: relative;background-color: white;}#sk-container-id-3 div.sk-parallel-item:first-child::after {align-self: flex-end;width: 50%;}#sk-container-id-3 div.sk-parallel-item:last-child::after {align-self: flex-start;width: 50%;}#sk-container-id-3 div.sk-parallel-item:only-child::after {width: 0;}#sk-container-id-3 div.sk-dashed-wrapped {border: 1px dashed gray;margin: 0 0.4em 0.5em 0.4em;box-sizing: border-box;padding-bottom: 0.4em;background-color: white;}#sk-container-id-3 div.sk-label label {font-family: monospace;font-weight: bold;display: inline-block;line-height: 1.2em;}#sk-container-id-3 div.sk-label-container {text-align: center;}#sk-container-id-3 div.sk-container {/* jupyter's `normalize.less` sets `[hidden] { display: none; }` but bootstrap.min.css set `[hidden] { display: none !important; }` so we also need the `!important` here to be able to override the default hidden behavior on the sphinx rendered scikit-learn.org. See: https://github.com/scikit-learn/scikit-learn/issues/21755 */display: inline-block !important;position: relative;}#sk-container-id-3 div.sk-text-repr-fallback {display: none;}</style><div id="sk-container-id-3" class="sk-top-container"><div class="sk-text-repr-fallback"><pre>NearestNeighbors(algorithm=&#x27;ball_tree&#x27;, n_neighbors=6)</pre><b>In a Jupyter environment, please rerun this cell to show the HTML representation or trust the notebook. <br />On GitHub, the HTML representation is unable to render, please try loading this page with nbviewer.org.</b></div><div class="sk-container" hidden><div class="sk-item"><div class="sk-estimator sk-toggleable"><input class="sk-toggleable__control sk-hidden--visually" id="sk-estimator-id-3" type="checkbox" checked><label for="sk-estimator-id-3" class="sk-toggleable__label sk-toggleable__label-arrow">NearestNeighbors</label><div class="sk-toggleable__content"><pre>NearestNeighbors(algorithm=&#x27;ball_tree&#x27;, n_neighbors=6)</pre></div></div></div></div></div>




```python
# Querying the model to find the nearest neighbors
distance, indices = model.kneighbors(model_data[['average_rating','num_of_pages','ratings_count','text_reviews_count','ratings_dist']])
```


```python
db = model_data.copy()
db['title'] =model_data['title']
db['indices'] = indices.tolist()
db['distance']= distance.tolist()
db.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>title</th>
      <th>average_rating</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>ratings_dist</th>
      <th>indices</th>
      <th>distance</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>4.57</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>4</td>
      <td>[0, 2114, 23, 1, 2116, 4415]</td>
      <td>[0.0, 16203.576685707387, 36051.17575120262, 5...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>4.49</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>4</td>
      <td>[1, 23, 2114, 0, 2116, 4415]</td>
      <td>[0.0, 28789.06240253232, 41426.26377448973, 57...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>4.42</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>4</td>
      <td>[2, 254, 8166, 839, 10614, 7824]</td>
      <td>[0.0, 77.3889714365038, 78.23977505080137, 85....</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>4.56</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>4</td>
      <td>[3, 4415, 307, 1462, 1, 1697]</td>
      <td>[0.0, 45651.31333291081, 80564.4579665804, 117...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>4.78</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>4</td>
      <td>[4, 1643, 7870, 4143, 5456, 3624]</td>
      <td>[0.0, 2022.5205749509694, 2100.808411088455, 2...</td>
    </tr>
  </tbody>
</table>
</div>




```python
import re
db.to_csv('BOOKSDB.csv',index=False)
class BookQuest:
    def __init__(self, dataframe, indices):
        self.df = dataframe
        self.indices = indices
        self.all_books_names = list(self.df["title"].values)
    
    def find_id(self,name):
        for index,string in enumerate(self.all_books_names):
            if re.search(name,string):
                index=index;
                break;
        return(index)

    def print_similar_books(self, query=None):
        if query:
            found_id = self.find_id(query)
            for id in self.indices[found_id][1:]:
                print(self.df.iloc[id]["title"])
db
recsys = BookQuest(db,db.indices)
recsys.print_similar_books("Harry")
```

    Animal Farm
    The Fellowship of the Ring (The Lord of the Rings  #1)
    Harry Potter and the Order of the Phoenix (Harry Potter  #5)
    Lord of the Flies
    Harry Potter and the Chamber of Secrets (Harry Potter  #2)


#### K-Mean Clustering

KMeans clustering is a type of unsupervised learning which groups unlabelled data. The goal is to find groups in data.
With this, I attepmt to find a relationship or groups between the rating count and average rating value.


```python
from sklearn.cluster import KMeans
from scipy.cluster.vq import kmeans, vq
from matplotlib.lines import Line2D
```


```python
trial = data[['average_rating', 'ratings_count']]
data_k = np.asarray([np.asarray(trial['average_rating']), np.asarray(trial['ratings_count'])]).T
```

I'll use the Elbow Curve method for the best way of finding the number of clusters for the data


```python
X = data_k
distortions = []
for k in range(2,30):
    k_means = KMeans(n_clusters = k)
    k_means.fit(X)
    distortions.append(k_means.inertia_)

fig = plt.figure(figsize=(15,10))
plt.plot(range(2,30), distortions, 'bx-')
plt.title("Elbow Curve");
```


    
![png](output_157_0.png)
    


From the above plot, we can see that the elbow lies around the value K=5, so that's what we will attempt it with


```python
#Computing K means with K = 5, thus, taking it as 5 clusters
centroids, _ = kmeans(data_k, 5)

#assigning each sample to a cluster
#Vector Quantisation:

idx, _ = vq(data_k, centroids)

```


```python
# some plotting using numpy's logical indexing
plt.figure(figsize=(15,10))
plt.plot(data_k[idx==0,0],data_k[idx==0,1],'or',#red circles
     data_k[idx==1,0],data_k[idx==1,1],'ob',#blue circles
     data_k[idx==2,0],data_k[idx==2,1],'oy', #yellow circles
     data_k[idx==3,0],data_k[idx==3,1],'om', #magenta circles
     data_k[idx==4,0],data_k[idx==4,1],'ok',#black circles
    
     
        
        
        
        
        )
plt.plot(centroids[:,0],centroids[:,1],'sg',markersize=8, )




circle1 = Line2D(range(1), range(1), color = 'red', linewidth = 0, marker= 'o', markerfacecolor='red')
circle2 = Line2D(range(1), range(1), color = 'blue', linewidth = 0,marker= 'o', markerfacecolor='blue')
circle3 = Line2D(range(1), range(1), color = 'yellow',linewidth=0,  marker= 'o', markerfacecolor='yellow')
circle4 = Line2D(range(1), range(1), color = 'magenta', linewidth=0,marker= 'o', markerfacecolor='magenta')
circle5 = Line2D(range(1), range(1), color = 'black', linewidth = 0,marker= 'o', markerfacecolor='black')

plt.legend((circle1, circle2, circle3, circle4, circle5)
           , ('Cluster 1','Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster 5'), numpoints = 1, loc = 0, )


plt.show()
```


    
![png](output_160_0.png)
    


We can see from the above plot, that because of two outliers, the whole clustering algortihm is skewed. Let's remove them and form inferences


```python
trial.idxmax()
```




    average_rating      624
    ratings_count     10336
    dtype: int64




```python
trial.drop(10336, inplace = True)
trial.drop(624, inplace = True)
```


```python
data_k = np.asarray([np.asarray(trial['average_rating']), np.asarray(trial['ratings_count'])]).T
```


```python
#Computing K means with K = 8, thus, taking it as 8 clusters
centroids, _ = kmeans(data_k, 5)

#assigning each sample to a cluster
#Vector Quantisation:

idx, _ = vq(data_k, centroids)
```


```python
# some plotting using numpy's logical indexing
plt.figure(figsize=(15,10))
plt.plot(data_k[idx==0,0],data_k[idx==0,1],'or',#red circles
     data_k[idx==1,0],data_k[idx==1,1],'ob',#blue circles
     data_k[idx==2,0],data_k[idx==2,1],'oy', #yellow circles
     data_k[idx==3,0],data_k[idx==3,1],'om', #magenta circles
     data_k[idx==4,0],data_k[idx==4,1],'ok',#black circles
    
     
        
        
        
        
        )
plt.plot(centroids[:,0],centroids[:,1],'sg',markersize=8, )




circle1 = Line2D(range(1), range(1), color = 'red', linewidth = 0, marker= 'o', markerfacecolor='red')
circle2 = Line2D(range(1), range(1), color = 'blue', linewidth = 0,marker= 'o', markerfacecolor='blue')
circle3 = Line2D(range(1), range(1), color = 'yellow',linewidth=0,  marker= 'o', markerfacecolor='yellow')
circle4 = Line2D(range(1), range(1), color = 'magenta', linewidth=0,marker= 'o', markerfacecolor='magenta')
circle5 = Line2D(range(1), range(1), color = 'black', linewidth = 0,marker= 'o', markerfacecolor='black')

plt.legend((circle1, circle2, circle3, circle4, circle5)
           , ('Cluster 1','Cluster 2', 'Cluster 3', 'Cluster 4', 'Cluster 5'), numpoints = 1, loc = 0, )


plt.show()
```


    
![png](output_166_0.png)
    


From the above plot, now we can see that once the whole system can be classified into clusters. As the count increases, the rating would end up near the cluster given above. The green squares are the centroids for the given clusters.

As the rating count seems to decrease, the average rating seems to become sparser, with higher volatility and less accuracy.

# cosine similarity


```python
model_data_prepro = model_data.drop('title', axis=1).copy()
```


```python
model_data_prepro = (model_data_prepro - model_data_prepro.mean())/model_data_prepro.std()
```


```python
model_data_prepro
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>average_rating</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>ratings_dist</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1.814317</td>
      <td>1.309070</td>
      <td>18.467346</td>
      <td>10.496898</td>
      <td>1.098780</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1.586080</td>
      <td>2.213147</td>
      <td>18.978212</td>
      <td>11.129455</td>
      <td>1.098780</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.386373</td>
      <td>0.064928</td>
      <td>-0.103210</td>
      <td>-0.115691</td>
      <td>1.098780</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1.785787</td>
      <td>0.409141</td>
      <td>20.635126</td>
      <td>13.886314</td>
      <td>1.098780</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2.413438</td>
      <td>9.760944</td>
      <td>0.208720</td>
      <td>-0.146737</td>
      <td>1.098780</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>0.359307</td>
      <td>0.728470</td>
      <td>-0.158113</td>
      <td>-0.202619</td>
      <td>1.098780</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>0.416366</td>
      <td>1.238569</td>
      <td>-0.152540</td>
      <td>-0.188649</td>
      <td>1.098780</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>0.074011</td>
      <td>0.326198</td>
      <td>-0.152211</td>
      <td>-0.173514</td>
      <td>-0.766513</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>-0.610699</td>
      <td>0.404993</td>
      <td>-0.152664</td>
      <td>-0.156439</td>
      <td>-0.766513</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>-0.068637</td>
      <td>-0.266843</td>
      <td>-0.158495</td>
      <td>-0.205724</td>
      <td>-0.766513</td>
    </tr>
  </tbody>
</table>
<p>11121 rows × 5 columns</p>
</div>




```python
label = model_data['title']
```


```python
from sklearn.metrics.pairwise import cosine_similarity
```


```python
res = cosine_similarity(model_data_prepro.iloc[0].values.reshape(1,-1), Y=model_data_prepro.values, dense_output=True)
```


```python
label[res.argsort()[0][-6:-1]]
```




    3       Harry Potter and the Prisoner of Azkaban (Harr...
    299                                   Memoirs of a Geisha
    1697                  The Hobbit  or There and Back Again
    591                                          Little Women
    1       Harry Potter and the Order of the Phoenix (Har...
    Name: title, dtype: object




```python
np.array([10,3,7,5])[np.argsort([10,3,7,5])]
```




    array([ 3,  5,  7, 10])



## Linear Regression Model

The aim of this section is to come up with a model for predicting the book ratings. We'll use linear regression to build a model that predicts book ratings.

Linear regression algorithm is a basic predictive analytics technique. There are two kinds of variables in a linear regression model:

The input or predictor variable is the variable(s) that help predict the value of the output variable. It is commonly referred to as X.
The output variable is the variable that we want to predict. It is commonly referred to as Y.

**Linear regression** is a statistical technique used to understand and predict the relationship between two or more variables. It's like drawing a line through a set of points on a graph to see if there's a pattern.

Let's say you have two variables: **X** and **Y**. You suspect that there might be a linear relationship between them, meaning that as X increases or decreases, Y also changes in a consistent way.

To find this relationship, you can use linear regression. The goal is to find the best-fitting line that represents the relationship between X and Y. This line can then be used to make predictions about Y based on known values of X.

In linear regression, the line is defined by two important components: the slope and the intercept. The slope represents how steep the line is, indicating how much Y changes for each unit change in X. The intercept represents where the line crosses the Y-axis.

To find the best-fitting line, the linear regression algorithm calculates the optimal values for the slope and intercept based on the given data. It does this by minimizing the difference between the predicted Y values from the line and the actual Y values in the data.

Once the line is determined, you can use it to make predictions. For example, if you know the value of X, you can plug it into the equation of the line and calculate the predicted value of Y.

It's important to note that linear regression makes some assumptions. It assumes that the relationship between X and Y is indeed linear, that there is a constant variance in the errors (residuals), and that the errors are normally distributed.


```python
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn import metrics

```


```python
linear_regressor_data = model_data.copy()
linear_regressor_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>title</th>
      <th>average_rating</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>ratings_dist</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>4.57</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>4</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>4.49</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>4</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>4.42</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>4</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>4.56</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>4.78</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>4</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>4.06</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>4</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>You Bright and Risen Angels</td>
      <td>4.08</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>4</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>3.96</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>3</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>Poor People</td>
      <td>3.72</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>3</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>Las aventuras de Tom Sawyer</td>
      <td>3.91</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
<p>11121 rows × 6 columns</p>
</div>



In linear regression, the variables involved are typically numerical. This means that both the dependent variable (Y) and the independent variables (X) are numerical and can take on continuous or discrete numeric values. So we are going to drop the strings values.


```python
X = linear_regressor_data.drop(['average_rating', 'title', 'ratings_dist'], axis = 1)
y = linear_regressor_data['average_rating']
```


```python
x, y
```




    (2764                                             Jane Eyre
     591                                           Little Women
     5270                                        The Book Thief
     1069                             The Giver (The Giver  #1)
     284                                          The Alchemist
     310                 The Da Vinci Code (Robert Langdon  #2)
     294                                        Of Mice and Men
     7309     The Lightning Thief (Percy Jackson and the Oly...
     5015                                      Romeo and Juliet
     2116                                     Lord of the Flies
     0        Harry Potter and the Half-Blood Prince (Harry ...
     2114                                           Animal Farm
     23       The Fellowship of the Ring (The Lord of the Ri...
     1        Harry Potter and the Order of the Phoenix (Har...
     4415     Harry Potter and the Chamber of Secrets (Harry...
     3        Harry Potter and the Prisoner of Azkaban (Harr...
     307                   Angels & Demons (Robert Langdon  #1)
     1462                                The Catcher in the Rye
     1697                   The Hobbit  or There and Back Again
     10336                              Twilight (Twilight  #1)
     Name: title, dtype: object,
     0        4.57
     1        4.49
     2        4.42
     3        4.56
     4        4.78
              ... 
     11118    4.06
     11119    4.08
     11120    3.96
     11121    3.72
     11122    3.91
     Name: average_rating, Length: 11121, dtype: float64)



Attributes are the independent variables whilst labels are dependent variables whose values are to be predicted.


```python
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 10)
```

___a bit of context___: This function is commonly used to split a dataset into training and testing sets, which are used for model training and evaluation, respectively.

Here's a breakdown of the code:

- **X** and **y** are the input features and target variable, respectively, that you want to split into training and testing sets.
- **test_size = 0.25** specifies that 25% of the data will be allocated for testing, while 75% will be used for training. You can adjust this value to allocate a different proportion for testing.
- **random_state = 10** sets a specific random seed value to ensure reproducibility. Using the same seed value will result in the same split each time you run the code. You can change this value or omit it to have a different random split each time.

After executing this code, you will have four new datasets:

- **X_train**: This will contain a subset of the input features (X) used for training the model.
- **X_test**: This will contain a different subset of the input features (X) used for testing the trained model's performance.
- **y_train**: This will contain a subset of the target variable (y) corresponding to the training set.
- **y_test**: This will contain a different subset of the target variable (y) corresponding to the testing set.

The purpose of splitting the data is to evaluate how well the trained model generalizes to unseen data. The model is trained on the X_train and y_train datasets and then evaluated on the X_test dataset by comparing the predicted values with the actual y_test values.

This separation helps assess the model's performance and identify if it is overfitting (performing well on the training data but poorly on new data) or underfitting (performing poorly on both training and new data).


```python
lr = LinearRegression()
lr.fit(X_train, y_train)
```




<style>#sk-container-id-1 {color: black;background-color: white;}#sk-container-id-1 pre{padding: 0;}#sk-container-id-1 div.sk-toggleable {background-color: white;}#sk-container-id-1 label.sk-toggleable__label {cursor: pointer;display: block;width: 100%;margin-bottom: 0;padding: 0.3em;box-sizing: border-box;text-align: center;}#sk-container-id-1 label.sk-toggleable__label-arrow:before {content: "▸";float: left;margin-right: 0.25em;color: #696969;}#sk-container-id-1 label.sk-toggleable__label-arrow:hover:before {color: black;}#sk-container-id-1 div.sk-estimator:hover label.sk-toggleable__label-arrow:before {color: black;}#sk-container-id-1 div.sk-toggleable__content {max-height: 0;max-width: 0;overflow: hidden;text-align: left;background-color: #f0f8ff;}#sk-container-id-1 div.sk-toggleable__content pre {margin: 0.2em;color: black;border-radius: 0.25em;background-color: #f0f8ff;}#sk-container-id-1 input.sk-toggleable__control:checked~div.sk-toggleable__content {max-height: 200px;max-width: 100%;overflow: auto;}#sk-container-id-1 input.sk-toggleable__control:checked~label.sk-toggleable__label-arrow:before {content: "▾";}#sk-container-id-1 div.sk-estimator input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-1 div.sk-label input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-1 input.sk-hidden--visually {border: 0;clip: rect(1px 1px 1px 1px);clip: rect(1px, 1px, 1px, 1px);height: 1px;margin: -1px;overflow: hidden;padding: 0;position: absolute;width: 1px;}#sk-container-id-1 div.sk-estimator {font-family: monospace;background-color: #f0f8ff;border: 1px dotted black;border-radius: 0.25em;box-sizing: border-box;margin-bottom: 0.5em;}#sk-container-id-1 div.sk-estimator:hover {background-color: #d4ebff;}#sk-container-id-1 div.sk-parallel-item::after {content: "";width: 100%;border-bottom: 1px solid gray;flex-grow: 1;}#sk-container-id-1 div.sk-label:hover label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-1 div.sk-serial::before {content: "";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: 0;}#sk-container-id-1 div.sk-serial {display: flex;flex-direction: column;align-items: center;background-color: white;padding-right: 0.2em;padding-left: 0.2em;position: relative;}#sk-container-id-1 div.sk-item {position: relative;z-index: 1;}#sk-container-id-1 div.sk-parallel {display: flex;align-items: stretch;justify-content: center;background-color: white;position: relative;}#sk-container-id-1 div.sk-item::before, #sk-container-id-1 div.sk-parallel-item::before {content: "";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: -1;}#sk-container-id-1 div.sk-parallel-item {display: flex;flex-direction: column;z-index: 1;position: relative;background-color: white;}#sk-container-id-1 div.sk-parallel-item:first-child::after {align-self: flex-end;width: 50%;}#sk-container-id-1 div.sk-parallel-item:last-child::after {align-self: flex-start;width: 50%;}#sk-container-id-1 div.sk-parallel-item:only-child::after {width: 0;}#sk-container-id-1 div.sk-dashed-wrapped {border: 1px dashed gray;margin: 0 0.4em 0.5em 0.4em;box-sizing: border-box;padding-bottom: 0.4em;background-color: white;}#sk-container-id-1 div.sk-label label {font-family: monospace;font-weight: bold;display: inline-block;line-height: 1.2em;}#sk-container-id-1 div.sk-label-container {text-align: center;}#sk-container-id-1 div.sk-container {/* jupyter's `normalize.less` sets `[hidden] { display: none; }` but bootstrap.min.css set `[hidden] { display: none !important; }` so we also need the `!important` here to be able to override the default hidden behavior on the sphinx rendered scikit-learn.org. See: https://github.com/scikit-learn/scikit-learn/issues/21755 */display: inline-block !important;position: relative;}#sk-container-id-1 div.sk-text-repr-fallback {display: none;}</style><div id="sk-container-id-1" class="sk-top-container"><div class="sk-text-repr-fallback"><pre>LinearRegression()</pre><b>In a Jupyter environment, please rerun this cell to show the HTML representation or trust the notebook. <br />On GitHub, the HTML representation is unable to render, please try loading this page with nbviewer.org.</b></div><div class="sk-container" hidden><div class="sk-item"><div class="sk-estimator sk-toggleable"><input class="sk-toggleable__control sk-hidden--visually" id="sk-estimator-id-1" type="checkbox" checked><label for="sk-estimator-id-1" class="sk-toggleable__label sk-toggleable__label-arrow">LinearRegression</label><div class="sk-toggleable__content"><pre>LinearRegression()</pre></div></div></div></div></div>




```python
ridge = Ridge(alpha=1)
ridge.fit(X_train, y_train)
```




<style>#sk-container-id-2 {color: black;background-color: white;}#sk-container-id-2 pre{padding: 0;}#sk-container-id-2 div.sk-toggleable {background-color: white;}#sk-container-id-2 label.sk-toggleable__label {cursor: pointer;display: block;width: 100%;margin-bottom: 0;padding: 0.3em;box-sizing: border-box;text-align: center;}#sk-container-id-2 label.sk-toggleable__label-arrow:before {content: "▸";float: left;margin-right: 0.25em;color: #696969;}#sk-container-id-2 label.sk-toggleable__label-arrow:hover:before {color: black;}#sk-container-id-2 div.sk-estimator:hover label.sk-toggleable__label-arrow:before {color: black;}#sk-container-id-2 div.sk-toggleable__content {max-height: 0;max-width: 0;overflow: hidden;text-align: left;background-color: #f0f8ff;}#sk-container-id-2 div.sk-toggleable__content pre {margin: 0.2em;color: black;border-radius: 0.25em;background-color: #f0f8ff;}#sk-container-id-2 input.sk-toggleable__control:checked~div.sk-toggleable__content {max-height: 200px;max-width: 100%;overflow: auto;}#sk-container-id-2 input.sk-toggleable__control:checked~label.sk-toggleable__label-arrow:before {content: "▾";}#sk-container-id-2 div.sk-estimator input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-2 div.sk-label input.sk-toggleable__control:checked~label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-2 input.sk-hidden--visually {border: 0;clip: rect(1px 1px 1px 1px);clip: rect(1px, 1px, 1px, 1px);height: 1px;margin: -1px;overflow: hidden;padding: 0;position: absolute;width: 1px;}#sk-container-id-2 div.sk-estimator {font-family: monospace;background-color: #f0f8ff;border: 1px dotted black;border-radius: 0.25em;box-sizing: border-box;margin-bottom: 0.5em;}#sk-container-id-2 div.sk-estimator:hover {background-color: #d4ebff;}#sk-container-id-2 div.sk-parallel-item::after {content: "";width: 100%;border-bottom: 1px solid gray;flex-grow: 1;}#sk-container-id-2 div.sk-label:hover label.sk-toggleable__label {background-color: #d4ebff;}#sk-container-id-2 div.sk-serial::before {content: "";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: 0;}#sk-container-id-2 div.sk-serial {display: flex;flex-direction: column;align-items: center;background-color: white;padding-right: 0.2em;padding-left: 0.2em;position: relative;}#sk-container-id-2 div.sk-item {position: relative;z-index: 1;}#sk-container-id-2 div.sk-parallel {display: flex;align-items: stretch;justify-content: center;background-color: white;position: relative;}#sk-container-id-2 div.sk-item::before, #sk-container-id-2 div.sk-parallel-item::before {content: "";position: absolute;border-left: 1px solid gray;box-sizing: border-box;top: 0;bottom: 0;left: 50%;z-index: -1;}#sk-container-id-2 div.sk-parallel-item {display: flex;flex-direction: column;z-index: 1;position: relative;background-color: white;}#sk-container-id-2 div.sk-parallel-item:first-child::after {align-self: flex-end;width: 50%;}#sk-container-id-2 div.sk-parallel-item:last-child::after {align-self: flex-start;width: 50%;}#sk-container-id-2 div.sk-parallel-item:only-child::after {width: 0;}#sk-container-id-2 div.sk-dashed-wrapped {border: 1px dashed gray;margin: 0 0.4em 0.5em 0.4em;box-sizing: border-box;padding-bottom: 0.4em;background-color: white;}#sk-container-id-2 div.sk-label label {font-family: monospace;font-weight: bold;display: inline-block;line-height: 1.2em;}#sk-container-id-2 div.sk-label-container {text-align: center;}#sk-container-id-2 div.sk-container {/* jupyter's `normalize.less` sets `[hidden] { display: none; }` but bootstrap.min.css set `[hidden] { display: none !important; }` so we also need the `!important` here to be able to override the default hidden behavior on the sphinx rendered scikit-learn.org. See: https://github.com/scikit-learn/scikit-learn/issues/21755 */display: inline-block !important;position: relative;}#sk-container-id-2 div.sk-text-repr-fallback {display: none;}</style><div id="sk-container-id-2" class="sk-top-container"><div class="sk-text-repr-fallback"><pre>Ridge(alpha=1)</pre><b>In a Jupyter environment, please rerun this cell to show the HTML representation or trust the notebook. <br />On GitHub, the HTML representation is unable to render, please try loading this page with nbviewer.org.</b></div><div class="sk-container" hidden><div class="sk-item"><div class="sk-estimator sk-toggleable"><input class="sk-toggleable__control sk-hidden--visually" id="sk-estimator-id-2" type="checkbox" checked><label for="sk-estimator-id-2" class="sk-toggleable__label sk-toggleable__label-arrow">Ridge</label><div class="sk-toggleable__content"><pre>Ridge(alpha=1)</pre></div></div></div></div></div>



_lr.fit(X_train, y_train)_ trains the linear regression model using the training data. The fit method fits the model by estimating the optimal values for the slope and intercept based on the provided input features (X_train) and target variable (y_train).

Next step is to use the test data to check accurately our algorithm predicts the percentage score.




```python
predictions = lr.predict(X_test)
predictions_ridge = ridge.predict(X_test)
```

Now compare the actual output values for X_test with the predicted values.




```python
pred = pd.DataFrame({'Actual': y_test.tolist(), 'Predicted': predictions.tolist()}).head(25)
pred.head(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Actual</th>
      <th>Predicted</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>4.05</td>
      <td>3.926118</td>
    </tr>
    <tr>
      <th>1</th>
      <td>3.75</td>
      <td>3.904260</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3.74</td>
      <td>3.896896</td>
    </tr>
    <tr>
      <th>3</th>
      <td>3.64</td>
      <td>3.893552</td>
    </tr>
    <tr>
      <th>4</th>
      <td>4.38</td>
      <td>3.890123</td>
    </tr>
    <tr>
      <th>5</th>
      <td>4.44</td>
      <td>3.881069</td>
    </tr>
    <tr>
      <th>6</th>
      <td>3.78</td>
      <td>3.947459</td>
    </tr>
    <tr>
      <th>7</th>
      <td>3.87</td>
      <td>3.942772</td>
    </tr>
    <tr>
      <th>8</th>
      <td>3.77</td>
      <td>3.888555</td>
    </tr>
    <tr>
      <th>9</th>
      <td>3.99</td>
      <td>3.902422</td>
    </tr>
  </tbody>
</table>
</div>




```python
# visualise the above comparison result
pred.plot(kind='bar', figsize=(13, 7));

```


    
![png](output_195_0.png)
    


Though the model is not very precise, the predicted percentages are close to the actual ones.



```python
pred_ridge = pd.DataFrame({'Actual': y_test.tolist(), 'Predicted': predictions_ridge.tolist()}).head(25)
plt.scatter(y_test, predictions_ridge);

```


    
![png](output_197_0.png)
    



```python
plt.scatter(y_train, ridge.predict(X_train));
```


    
![png](output_198_0.png)
    


#### Model evalutation

To Evaluate the model performance we use the common evaluation metrics.

#### Mean Absolute Error (MAE):

MAE represents the average absolute difference between the predicted values and the actual values.
It is calculated by taking the average of the absolute differences between each predicted value and its corresponding actual value.
MAE is useful because it gives equal weight to all errors without considering their direction (positive or negative).
The formula for MAE is:
\$ MAE = (1/n) * Σ|y_{pred} - y_{actual}| $


#### Mean Squared Error (MSE):

MSE measures the average squared difference between the predicted values and the actual values.
It is calculated by taking the average of the squared differences between each predicted value and its corresponding actual value.
Squaring the differences emphasizes larger errors and penalizes outliers more heavily.
The formula for MSE is:
\$\ MSE = (1/n) * Σ(y_{pred} - y_{actual})^2$

#### Root Mean Squared Error (RMSE):

RMSE is the square root of MSE and provides a measure of the average magnitude of the errors in the same units as the target variable.
It is commonly used because it has the same unit of measurement as the dependent variable, making it easier to interpret.
The formula for RMSE is:
RMSE = \$\sqrt{(MSE)}$


In summary, MAE, MSE, and RMSE all provide measures of the error between predicted and actual values. MAE is the simplest and gives equal weight to all errors, while MSE and RMSE emphasize larger errors due to the squaring operation. RMSE is particularly useful when you want the error metric to be in the same unit as the target variable, making it more interpretable.

When comparing models or evaluating the performance of a single model, lower values of MAE, MSE, and RMSE indicate better accuracy and prediction quality, as they reflect smaller errors between the predicted and actual values.


```python
print('MAE:', metrics.mean_absolute_error(y_test, predictions))
print('MSE:', metrics.mean_squared_error(y_test, predictions))
print('RMSE:', np.sqrt(metrics.mean_squared_error(y_test, predictions)))
print('R Square:', metrics.r2_score(y_test, predictions))

```

    MAE: 0.22813304450904032
    MSE: 0.105447356945243
    RMSE: 0.3247265879863289
    R Square: 0.012610205361354931



```python
print('MAE:', metrics.mean_absolute_error(y_test, predictions_ridge))
print('MSE:', metrics.mean_squared_error(y_test, predictions_ridge))
print('RMSE:', np.sqrt(metrics.mean_squared_error(y_test, predictions_ridge)))
print('R Square:', metrics.r2_score(y_test, predictions_ridge))

```

    MAE: 0.22813304450538333
    MSE: 0.10544735694179279
    RMSE: 0.3247265879810164
    R Square: 0.012610205393662088



```python
test = lr.predict([(870,2153167,29221)])
```


```python
pred = pd.DataFrame({'Predicted': test.tolist()}).head(25)
pred.head(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Predicted</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>4.271425</td>
    </tr>
  </tbody>
</table>
</div>




```python
data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bookID</th>
      <th>title</th>
      <th>authors</th>
      <th>average_rating</th>
      <th>isbn</th>
      <th>isbn13</th>
      <th>language_code</th>
      <th>num_of_pages</th>
      <th>ratings_count</th>
      <th>text_reviews_count</th>
      <th>publication_date</th>
      <th>publisher</th>
      <th>bin</th>
      <th>ratings_dist</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Harry Potter and the Half-Blood Prince (Harry ...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.57</td>
      <td>0439785960</td>
      <td>9780439785969</td>
      <td>eng</td>
      <td>652</td>
      <td>2095690</td>
      <td>27591</td>
      <td>2006-09-16</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Harry Potter and the Order of the Phoenix (Har...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.49</td>
      <td>0439358078</td>
      <td>9780439358071</td>
      <td>eng</td>
      <td>870</td>
      <td>2153167</td>
      <td>29221</td>
      <td>2004-09-01</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>Harry Potter and the Chamber of Secrets (Harry...</td>
      <td>J.K. Rowling</td>
      <td>4.42</td>
      <td>0439554896</td>
      <td>9780439554893</td>
      <td>eng</td>
      <td>352</td>
      <td>6333</td>
      <td>244</td>
      <td>2003-11-01</td>
      <td>Scholastic</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5</td>
      <td>Harry Potter and the Prisoner of Azkaban (Harr...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.56</td>
      <td>043965548X</td>
      <td>9780439655484</td>
      <td>eng</td>
      <td>435</td>
      <td>2339585</td>
      <td>36325</td>
      <td>2004-05-01</td>
      <td>Scholastic Inc.</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8</td>
      <td>Harry Potter Boxed Set  Books 1-5 (Harry Potte...</td>
      <td>J.K. Rowling/Mary GrandPré</td>
      <td>4.78</td>
      <td>0439682584</td>
      <td>9780439682589</td>
      <td>eng</td>
      <td>2690</td>
      <td>41428</td>
      <td>164</td>
      <td>2004-09-13</td>
      <td>Scholastic</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>11118</th>
      <td>45631</td>
      <td>Expelled from Eden: A William T. Vollmann Reader</td>
      <td>William T. Vollmann/Larry McCaffery/Michael He...</td>
      <td>4.06</td>
      <td>1560254416</td>
      <td>9781560254416</td>
      <td>eng</td>
      <td>512</td>
      <td>156</td>
      <td>20</td>
      <td>2004-12-21</td>
      <td>Da Capo Press</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>11119</th>
      <td>45633</td>
      <td>You Bright and Risen Angels</td>
      <td>William T. Vollmann</td>
      <td>4.08</td>
      <td>0140110879</td>
      <td>9780140110876</td>
      <td>eng</td>
      <td>635</td>
      <td>783</td>
      <td>56</td>
      <td>1988-12-01</td>
      <td>Penguin Books</td>
      <td>(4, 5]</td>
      <td>Between 4 and 5</td>
    </tr>
    <tr>
      <th>11120</th>
      <td>45634</td>
      <td>The Ice-Shirt (Seven Dreams #1)</td>
      <td>William T. Vollmann</td>
      <td>3.96</td>
      <td>0140131965</td>
      <td>9780140131963</td>
      <td>eng</td>
      <td>415</td>
      <td>820</td>
      <td>95</td>
      <td>1993-08-01</td>
      <td>Penguin Books</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
    <tr>
      <th>11121</th>
      <td>45639</td>
      <td>Poor People</td>
      <td>William T. Vollmann</td>
      <td>3.72</td>
      <td>0060878827</td>
      <td>9780060878825</td>
      <td>eng</td>
      <td>434</td>
      <td>769</td>
      <td>139</td>
      <td>2007-02-27</td>
      <td>Ecco</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
    <tr>
      <th>11122</th>
      <td>45641</td>
      <td>Las aventuras de Tom Sawyer</td>
      <td>Mark Twain</td>
      <td>3.91</td>
      <td>8497646983</td>
      <td>9788497646987</td>
      <td>spa</td>
      <td>272</td>
      <td>113</td>
      <td>12</td>
      <td>2006-05-28</td>
      <td>Edimat Libros</td>
      <td>(3, 4]</td>
      <td>Between 3 and 4</td>
    </tr>
  </tbody>
</table>
<p>11121 rows × 14 columns</p>
</div>



predittore medio se é uguale non sto facendo nulla
predittore moda


```python

```
